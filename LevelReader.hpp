#pragma once

#include <memory>
#include <d3dx9core.h>
#include <irrklang.h>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include "Brick.hpp"

using namespace std;
using namespace irrklang;

struct LevelData
{
	LevelData()
	: mLevelName(L""),
	  mDescription(L""),
	  mAuthor(L""),
	  mMusicFilename(L""),
	  mPlayStart(0)
	{}
	~LevelData()
	{
		for(unsigned i = 0; i < mBricks.size(); i++)
		{
			if(mBricks[i])
			{
				delete mBricks[i];
				mBricks[i] = 0;
			}
		}
		mBricks.clear();
	}

	wstring			mLevelName;
	wstring			mDescription;
	wstring			mAuthor;
	wstring			mMusicFilename;
	long			mPlayStart;
	vector<Brick*>	mBricks;
};

enum IO_Result
{
	IO_Dummy,
	IO_Success,
	IO_SectionNotFound,
	IO_LabelNotFound,
	IO_BadFileName,
	IO_CantOpenFile
};

class LevelReader
{
private:
	wifstream mIFS;
	wstring mFileName;

	void MovePastWhiteSpace();
	void RemoveSpecialCharacters(wstring& string);
	void RemoveSpaces(wstring& string);
	void RemoveLeadingSpaces(wstring& string);
	IO_Result MoveToSection(const wstring& section);
	vector<wstring> GetLineAsString();
	D3DXVECTOR3 GetLineAsVector();
	template <typename T>
	void StringTo(wstring& string, T& out);

public:
	// Constructors and Destructors
	LevelReader(wstring fileName);
	LevelReader();
	~LevelReader();

	IO_Result OpenLevelFile(wstring fileName);
	IO_Result GetStringFromSection(const wstring& section, const wstring& label, wstring& out);
	IO_Result GetIntFromSection(const wstring& section, const wstring& label, int& out);
	IO_Result GetLongFromSection(const wstring& section, const wstring& label, long& out);
	IO_Result GetVectorsFromSection(const wstring& section, vector<D3DXVECTOR3>& vectors);
};

auto_ptr<LevelData> BuildLevelFromFile(wstring fileName);