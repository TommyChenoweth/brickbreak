#include <d3dx9core.h>
#include <math.h>
#include "BoundingSphere.hpp"

// Constructors and Destructor
BoundingSphere::BoundingSphere(D3DXVECTOR3 center, float radius)
: mCenter(center),
  mRadius(1.0f)
{
	SetRadius(radius);
}
BoundingSphere::~BoundingSphere()
{
}

// Accessors and Mutators
D3DXVECTOR3 BoundingSphere::GetCenter() const
{
	return mCenter;
}
void BoundingSphere::SetCenter(D3DXVECTOR3 center)
{
	mCenter = center;
}

float BoundingSphere::GetRadius() const
{
	return mRadius;
}
void BoundingSphere::SetRadius(float radius)
{
	0 < radius ? mRadius = radius : 0;
}

void BoundingSphere::BuildSphereFromTopLeft(D3DXVECTOR3 topLeft)
{
	mCenter = topLeft + D3DXVECTOR3(mRadius, mRadius, 0);
}

// Non-Member Functions
bool IsPtInSphere(const D3DXVECTOR3& pt, const BoundingSphere& bSphere)
{
	return (pow(pt.x - bSphere.GetCenter().x, 2) + pow(pt.y - bSphere.GetCenter().y, 2)) <= pow(bSphere.GetRadius(), 2);
}