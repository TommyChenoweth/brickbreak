#pragma once

#include "Ball.hpp"
#include "BoundingBox.hpp"
#include <d3dx9core.h>
#include <irrKlang.h>
#include "Paddle.hpp"
#include "SharedSprite.hpp"
#include <string>

using namespace std;
using namespace irrklang;

class PowerMod : public SharedSprite<PowerMod>
{
private:
	BoundingBox mBounds;

	static const float mWidth;
	static const float mHeight;

	static ISoundSource* mActivateSound;
	static ISoundEngine* mIKEngine;

	unsigned mFrame; // Temporary
	RECT mFrames[4]; // Temporary

public:
	// Constructors and Destructor
	PowerMod(D3DXVECTOR3 position);
	PowerMod();
	~PowerMod();

	// Accessors and Mutators
	// mPosition
	void SetPosition(D3DXVECTOR3 position);
	// mBounds
	BoundingBox GetBounds();
	// mFrame
	void SetFrame(unsigned frame);

	void Draw();
	void (*Action)(Ball*, Paddle*);

	static void LoadSFX(ISoundEngine* iKEngine, string fileName);
	void PlaySFX();
	static void DropSFX();
};

bool PowerModWallCollision(PowerMod& mod, const RECT& walls);
PowerMod GetRandomPowerMod();