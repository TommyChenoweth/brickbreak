#pragma once

#include <irrKlang.h>
#include <string>

using namespace irrklang;
using namespace std;

template <typename T>
class SharedSound
{
private:
	static ISoundSource* mSound;
	static ISoundEngine* mIKEngine;

public:
	// Constructors and Destructors
	SharedSound();
	~SharedSound();

	void PlaySFX();
	static void LoadSound(ISoundEngine* iKEngine, string filename);
	static void DropSound();
};

template <typename T>
ISoundSource* SharedSound<T>::mSound = 0;
template <typename T>
ISoundEngine* SharedSound<T>::mIKEngine = 0;

// Constructors and Destructors
template <typename T>
SharedSound<T>::SharedSound()
{
}
template <typename T>
SharedSound<T>::~SharedSound()
{
}

template <typename T>
void SharedSound<T>::PlaySFX()
{
	if(mIKEngine)
		mIKEngine->play2D(mSound, false, false, false, false);
}
template <typename T>
void SharedSound<T>::LoadSound(ISoundEngine* iKEngine, string filename)
{
	mSound = iKEngine->addSoundSourceFromFile(filename.c_str(), ESM_NO_STREAMING, true);
	mIKEngine = iKEngine;
}
template <typename T>
void SharedSound<T>::DropSound()
{
	if(mSound)
	{
		mSound->drop();
		mSound = 0;
	}
}