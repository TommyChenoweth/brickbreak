#include <math.h>
#include <iostream>
#include <cstdio>
#include "CircleEquation.hpp"
#include "Solution.hpp"

using namespace std;

// Constuctors and Destructor
CircleEquation::CircleEquation(float r, float h, float k)
: mR(1.0f),
  mX(1.0f),
  mH(h),
  mY(1.0f),
  mK(k),
  mCenter(h, k, 0.0f)
{
	SetR(r);
}
CircleEquation::CircleEquation()
: mR(1.0f),
  mX(1.0f),
  mH(0.0f),
  mY(1.0f),
  mK(0.0f),
  mCenter(mH, mK, 0.0f)
{
}
CircleEquation::~CircleEquation()
{
}

// Accessors and Mutators
// mR
float CircleEquation::GetR() const
{
	return mR;
}
void CircleEquation::SetR(float r)
{
	0 < r ? mR = r : 0;
}
// mX
float CircleEquation::GetX() const
{
	return mX;
}
void CircleEquation::SetX(float x)
{
	mX = x;
}
// mH
float CircleEquation::GetH() const
{
	return mH;
}
void CircleEquation::SetH(float h)
{
	mH = h;
	mCenter.x = mH;
}
// mY
float CircleEquation::GetY() const
{
	return mY;
}
void CircleEquation::SetY(float y)
{
	mY = y;
}
// mK
float CircleEquation::GetK() const
{
	return mK;
}
void CircleEquation::SetK(float k)
{
	mK = k;
	mCenter.y = mK;
}
// mCenter
D3DXVECTOR3 CircleEquation::GetCenter() const
{
	return mCenter;
}

// Overloaded Operators
ostream& operator<<(ostream& os, CircleEquation& circle)
{
	return os << circle.GetR() << "^2 = (X-" << circle.GetH() << ")^2 + (Y-" << circle.GetK() << ")^2";
}

// Non-Member Functions
pair<Solution, Solution> SolveForY(const CircleEquation& circle)
{
	float tmp(0.0f);
	tmp = -pow(circle.GetX()-circle.GetH(), 2) + pow(circle.GetR(), 2);
	pair<Solution, Solution> solutions(Solution(0.0f), Solution(0.0f));
	if(tmp < 0)
	{
		solutions.first.SetImaginary();
		solutions.second.SetImaginary();
		wprintf(L"\n*IMAGINARY*");
		return solutions;
	}
	solutions.first.SetSolution(sqrt(tmp) + circle.GetK());
	solutions.second.SetSolution(-sqrt(tmp) + circle.GetK());
	return solutions;
}
pair<Solution, Solution> SolveForX(const CircleEquation& circle)
{
	float tmp(0.0f);
	tmp = -pow(circle.GetY()-circle.GetK(), 2) + pow(circle.GetR(), 2);
	pair<Solution, Solution> solutions(Solution(0.0f), Solution(0.0f));
	if(tmp < 0)
	{
		solutions.first.SetImaginary();
		solutions.second.SetImaginary();
		wprintf(L"\n*IMAGINARY*");
		return solutions;
	}
	solutions.first.SetSolution(sqrt(tmp) + circle.GetH());
	solutions.second.SetSolution(-sqrt(tmp) + circle.GetH());
	return solutions;
}

bool IsPointInCircle(const D3DXVECTOR3& point, const CircleEquation& circle)
{
	return ( pow(point.x - circle.GetCenter().x, 2) + pow(point.y - circle.GetCenter().y, 2) ) <= pow(circle.GetR(), 2);
}