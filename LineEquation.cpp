#include <d3dx9core.h>
#include "LineEquation.hpp"
#include <cassert>

using namespace std;

// Constructors and Destructor
LineEquation::LineEquation(float m, float b)
: mY(0.0f),
  mM(1.0f),
  mX(0.0f),
  mB(b)
{
	SetM(m);
}
LineEquation::LineEquation()
: mY(0.0f),
  mM(1.0f),
  mX(0.0f),
  mB(0.0f)
{
}
LineEquation::~LineEquation()
{
}

// Accessors and Mutators
// mY
float LineEquation::GetY() const
{
	return mY;
}
void LineEquation::SetY(float y)
{
	mY = y;
}
// mM
float LineEquation::GetM() const
{
	return mM;
}
void LineEquation::SetM(float m)
{
	m != 0.0f ? mM = m : 0;
}
// mX
float LineEquation::GetX() const
{
	return mX;
}
void LineEquation::SetX(float x)
{
	mX = x;
}
// mB
float LineEquation::GetB() const
{
	return mB;
}
void LineEquation::SetB(float b)
{
	mB = b;
}

// Overloaded Operators
ostream& operator<<(ostream& os, LineEquation& le)
{
	return os << "Y = " << le.GetM() << "X + " << le.GetB();
}

// Non-member functions
float SolveForY(const LineEquation& line)
{
	return line.GetM()*line.GetX() + line.GetB();
}
float SolveForX(const LineEquation& line)
{
	assert(line.GetM() != 0.0f);
	return (line.GetY()-line.GetB()) / line.GetM();
}
float SolveForB(const LineEquation& line)
{
	return line.GetY() - (line.GetM()*line.GetX());
}
float DetermineSlope(D3DXVECTOR3 pointA, D3DXVECTOR3 pointB)
{
	return (pointB.y - pointA.y) / (pointB.x - pointA.x);
}