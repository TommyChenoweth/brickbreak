#pragma once

#include <d3d9.h>
#include <D3dx9core.h>
#include <D3D9Types.h>
#include <windows.h>
#include <string>
#include <vector>

class GameTimer;

using namespace std;

class D3DApp
{
private:
	D3DPRESENT_PARAMETERS		mD3DPresentParams;

	UINT						mAdapter;
	DWORD						mRequestedVP;
	DWORD						mDevBehaviorFlags;
	D3DDEVTYPE					mDevType;
	D3DFORMAT					mAdptrFormat;
	D3DFORMAT					mBBFormat;

	HINSTANCE					mInstance;
	wstring						mTitle;

	void InitMainWindow();

	void InitD3D();
	HRESULT CreateD3DObject();
	HRESULT VerifyHALAndFormatSupport();
	HRESULT VerifyHardwareVertexProcessing();
	void SetPresentParams();
	HRESULT CreateDeviceInterface();

	void CreateSprite();
	void CreateFont();

protected:
	IDirect3D9*					mD3DObject;
	IDirect3DDevice9*			mD3DDevice;
	ID3DXSprite*				mD3DSprite;
	ID3DXFont*					mD3DFont;

	bool						mAppPaused;

	unsigned					mWindowWidth;
	unsigned					mWindowHeight;

	GameTimer*					mTimer;

	HWND						mWindow;

public:
	// Constructors and Destructor
	D3DApp(HINSTANCE instance, unsigned width, unsigned height, wstring title);
	virtual ~D3DApp();

	virtual void OnLostDevice();
	virtual void OnResetDevice();
	virtual bool IsDeviceLost();
	virtual int Run();
	virtual void UpdateScene(float dt);
	virtual void DrawScene();
	virtual LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam);
	virtual LRESULT UserInput(UINT msg, WPARAM wParam, LPARAM lParam);
	virtual void EnableFullScreenMode(bool enable);
};