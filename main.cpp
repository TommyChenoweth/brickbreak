#include <windows.h>
#include <cstdio>
#include "DXUtil.hpp"
#include "BrickBreak.hpp"

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check for debug builds.
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );

		AllocConsole() ;
		AttachConsole( GetCurrentProcessId() ) ;
		HANDLE conOut = GetStdHandle( STD_OUTPUT_HANDLE );
		COORD c;
		c.X = 80;
		c.Y = 1600;
		SetConsoleScreenBufferSize( conOut, c );
		freopen( "CON", "w", stdout ) ;
	#endif

	BrickBreak app(hInstance, 704, 704, L"A Title Is You");
	
	return app.Run();
}