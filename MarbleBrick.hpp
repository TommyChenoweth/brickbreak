#pragma once

#include "SpriteBrick.hpp"

class MarbleBrick : public SpriteBrick<MarbleBrick>
{
private:
	unsigned					mTimesStruck;
	unsigned					mStrikesToDestroy;
	unsigned					mFrame;
	unsigned					mCurrFrame;
	double						mTimePassed;
	static float				mDT;
	static IDirect3DTexture9*	mCracksTex;
	static const unsigned		mCrackRows;
	static const unsigned		mCrackCols;
	static const unsigned		mTotalFrames;

	void AnimateToNextFrame();

public:
	// Constructors and Destructors
	MarbleBrick();
	~MarbleBrick();

	int Struck();
	void Reset();
	void Draw();

	static void LoadTexture(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring filename);
	static void ReleaseTexture();

	static void UpdateDT(float dt);
};