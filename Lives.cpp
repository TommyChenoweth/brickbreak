#include "Lives.hpp"

// Constructors and Destructors
Lives::Lives(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, unsigned numLives)
: Sprite(d3dDevice, d3dSprite, L"Graphics/ball.dds"),
  mFilename(L"Graphics/ball.dds"),
  mStartingLives(numLives),
  mCurrLives(mStartingLives)
{
}
Lives::~Lives()
{
}

// Accessors and Mutators
// mNumLives
void Lives::AddLife()
{
	mCurrLives++;
}
void Lives::RemoveLife()
{
	if(mCurrLives != 0)
		mCurrLives--;
}
void Lives::ResetLives()
{
	mCurrLives = mStartingLives;
}
unsigned Lives::GetNumLives()
{
	return mCurrLives;
}

/************************************************
Function:
	void Lives::Draw()
Objective: 
	The goal of this function is to draw a 
	horizontal sequence of balls - the length of 
	which corresponds to the total number of 
	lives.
*************************************************/
void Lives::Draw()
{
	static D3DXVECTOR3  currPosition(0.0f, 0.0f, 0.0f);
	currPosition = mPosition;

	for(unsigned i = 0; i < mCurrLives; i++)
	{
		mD3DSprite->Draw(
			mD3DTex,
			0,
			0,
			&currPosition,
			D3DCOLOR_XRGB(255, 255, 255) );

		currPosition.x += 20.0f;
	}
}