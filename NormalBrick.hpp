#pragma once

#include "SpriteBrick.hpp"

class NormalBrick : public SpriteBrick<NormalBrick>
{
private:
public:
	// Constructors and Destructors
	NormalBrick(unsigned width, unsigned height);
	~NormalBrick();

	int Struck();
};