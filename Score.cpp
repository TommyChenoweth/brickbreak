#include <cassert>
#include "DXUtil.hpp"
#include "Score.hpp"
#include "Sprite.hpp"

#include <cstdio>

// Constructors and Destructor
Score::Score(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring fileName, float textureWidth, float textureHeight, float charWidth, float charHeight)
: Sprite(d3dDevice, d3dSprite, fileName),
  mTexWidth(textureWidth),
  mTexHeight(textureHeight),
  mCharWidth(charWidth),
  mCharHeight(charHeight),
  mScore(0),
  mMaxDigits(10),
  mDigits(0),
  mFramesForDigits(0),
  mTtlWidth(mMaxDigits * mCharWidth)
{
	const float borderWidth = 5.0f;
	const float borderHeight = 3.0f;
	SetPosition(D3DXVECTOR3(704.0f - borderWidth - mTtlWidth, 0.0f + borderHeight, 0.0f));

	mDigits = new int[mMaxDigits];
	for(unsigned i = 0; i < mMaxDigits; i++)
		mDigits[i] = 0;

	mFramesForDigits = new RECT[10];
	for(unsigned i = 0; i < 10; i++)
		mFramesForDigits[i] = GetFrameForDigit(i);

	GenDigitsFromScore();
}
Score::~Score()
{
	DeleteAPtr(mDigits);
	DeleteAPtr(mFramesForDigits);
}

// Private Functions
void Score::GenDigitsFromScore()
{
	unsigned long score(mScore);
	unsigned currDigit(0);
	// Reset all digits to zero.
	for(unsigned i = 0; i < mMaxDigits; i++)
		mDigits[i] = 0;
	while(0 < score && currDigit < mMaxDigits)
	{
		mDigits[currDigit] = score % 10;
		currDigit++;
		score /= 10;
	}
}
RECT Score::GetFrameForDigit(unsigned digit)
{
	assert(0 <= digit && digit < 10);
	static const unsigned CHARS_PER_ROW = static_cast<unsigned>(mTexWidth/mCharWidth);
	RECT frame = {0};
	frame.left = (digit % CHARS_PER_ROW) * static_cast<unsigned>(mCharWidth);
	frame.right = static_cast<unsigned>(frame.left + mCharWidth);
	frame.top = static_cast<unsigned>((digit / CHARS_PER_ROW) * mCharHeight);
	frame.bottom = static_cast<unsigned>(frame.top + mCharHeight);
	return frame;
}

// Accessors and Mutators
void Score::SetScore(unsigned long score)
{
	mScore = score;
	GenDigitsFromScore();
}
long Score::GetScore()
{
	return mScore;
}
void Score::AddToScore(long numToAdd)
{
	if(0 <= (static_cast<long>(mScore) + numToAdd))
		mScore += numToAdd;
	else
		mScore = 0;
	GenDigitsFromScore();
}

void Score::Draw()
{
	static D3DXVECTOR3 currPos(0.0f, 0.0f, 0.0f);
	currPos = mPosition;

	for(unsigned i = 0; i < mMaxDigits; i++)
	{
		currPos.x = mPosition.x + (i * mCharWidth);
		HRESULT drawSprite = mD3DSprite->Draw(
			mD3DTex,
			&mFramesForDigits[ mDigits[(mMaxDigits-1)-i] ],
			0,
			&currPos,
			D3DCOLOR_XRGB(255, 255, 255));
		switch(drawSprite)
		{
		case D3DERR_INVALIDCALL:
			ErrorBox(L"Can't draw sprite: Invalid parameter(s).");
			HR(drawSprite);
			break;
		case D3DXERR_INVALIDDATA:
			ErrorBox(L"Can't draw sprite: Data corrupt/incorrect.");
			HR(drawSprite);
			break;
		}
	}
}