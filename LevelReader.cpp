#include "boost\algorithm\string.hpp"
#include "LevelReader.hpp"
#include "MarbleBrick.hpp"
#include "NormalBrick.hpp"
#include <sstream>
#include <string>
#include "TitaniumBrick.hpp"

using namespace std;
using namespace boost::algorithm;

// Constructors and Destructors
LevelReader::LevelReader(wstring fileName)
: mFileName(fileName)
{
	OpenLevelFile(mFileName);
}

LevelReader::LevelReader()
: mFileName(L"")
{
}

LevelReader::~LevelReader()
{
	if(mIFS) mIFS.close();
}

void LevelReader::MovePastWhiteSpace()
{
	static const char SPACE = L' ';
	static const char TAB = L'\t';
	static const char NEWLINE = L'\n';

	while(mIFS.peek() == SPACE || mIFS.peek() == TAB || mIFS.peek() == NEWLINE)
		mIFS.seekg(0L, ios_base::cur);
}

void LevelReader::RemoveSpecialCharacters(wstring& string)
{
	static const wstring specialChars[] = {L"[", L"]", L"\"", L"(", L")"};
	static const int numChars = sizeof(specialChars)/sizeof(wstring(L" "));

	for(int i = 0; i < numChars; i++)
		erase_all(string, specialChars[i]);
}

void LevelReader::RemoveSpaces(wstring& string)
{
	static const wstring SPACE = L" ";
	erase_all(string, SPACE);
}

void LevelReader::RemoveLeadingSpaces(wstring& string)
{
	static const wchar_t SPACE = L' ';
	while(string[0] == SPACE)
		string.erase(0, 1);
}

IO_Result LevelReader::MoveToSection(const wstring& section)
{
	// Move to the beginning of the file.
	mIFS.seekg(0, ios::beg);

	// Ensure that the stream hasn't encountered any errors. If it has, reopen the file
	// in an attempt to fix them.
	int pos(mIFS.tellg());
	if(pos != 0)
	{
		mIFS.close();
		mIFS.clear();
		OpenLevelFile(mFileName);
	}

	wprintf(L"Searching for: %s\n", section.c_str());

	while(!mIFS.eof())
	{
		// If a section is found...
		if(mIFS.peek() == L'[')
		{
			// Read the section in.
			const int maxBufferSize(256);
			wstring data(L" ", maxBufferSize);
			mIFS.getline(&data[0], data.size());
			data.resize(mIFS.gcount()-1);
			wprintf(L"\t%s\n", data.c_str());

			RemoveSpecialCharacters(data);

			// Is this the desired section?
			static const int STRINGS_ARE_EQUAL(0);
			if(section.compare(data) == STRINGS_ARE_EQUAL)
				return IO_Success;
		}

		// Move to the next line.
		const int maxBufferSize(256);
		wstring data(L" ", maxBufferSize);
		mIFS.getline(&data[0], data.size());
		if(section.compare(L"Brick.Marble") == 0 && !mIFS.eof())
		{
			data.resize(mIFS.gcount()-1);
			wprintf(L"\t%s\n", data.c_str());
		}
	}
	return IO_SectionNotFound;
}

vector<wstring> LevelReader::GetLineAsString()
{
	// Read the line in.
	const int maxBufferSize(256);
	wstring data(L" ", maxBufferSize);
	mIFS.getline(&data[0], data.size());
	data.resize(mIFS.gcount());

	// Split the line about the equal sign.
	vector<wstring> labelAndData;
	split(labelAndData, data, is_any_of(L"="));

	// Remove special characters.
	for(unsigned i = 0; i < labelAndData.size(); i++)
		RemoveSpecialCharacters(labelAndData[i]);

	// Remove all spaces from the label.
	RemoveSpaces(labelAndData[0]);

	// Remove leading spaces from the data.
	RemoveLeadingSpaces(labelAndData[1]);

	return labelAndData;
}

D3DXVECTOR3 LevelReader::GetLineAsVector()
{
	// Read the line in.
	const int maxBufferSize(256);
	wstring data(L" ", maxBufferSize);
	mIFS.getline(&data[0], data.size());
	data.resize(mIFS.gcount()-1);

	// Split the line about the comma.
	vector<wstring> values;
	split(values, data, is_any_of(L","));

	// Remove special characters and spaces.
	RemoveSpecialCharacters(values[0]);
	RemoveSpaces(values[0]);
	RemoveSpecialCharacters(values[1]);
	RemoveSpaces(values[1]);

	// Fill out a vector with the retrieved data.
	D3DXVECTOR3 vector(0.0f, 0.0f, 0.0f);
	StringTo(values[0], vector.x);
	StringTo(values[1], vector.y);

	return vector;
}

template <typename T>
void LevelReader::StringTo(wstring& string, T& out)
{
	static wstringstream wss;
	wss.clear();
	wss << string;
	wss >> out;
}

IO_Result LevelReader::OpenLevelFile(wstring fileName)
{
	if(mIFS.is_open())
		mIFS.close();

	mFileName = fileName;

	try
	{
		mIFS.open(&mFileName[0], ios_base::in);
	}
	catch(exception e)
	{
		ErrorBox(L"Failed to open the file.");
		return IO_CantOpenFile;
	}
	return IO_Success;
}

IO_Result LevelReader::GetStringFromSection(const wstring& section, const wstring& label, wstring& out)
{
	// Is there an open file?
	if(!mIFS.is_open())
	{
		// No. Try opening the file again.
		OpenLevelFile(mFileName);
		if(!mIFS.is_open())
			return IO_CantOpenFile;
	}

	// Search the file for the section.
	if(MoveToSection(section) == IO_SectionNotFound)
	{
		// The section wasn't found.
		return IO_SectionNotFound;
	}

	// There may be white space between the section header and the first label.
	MovePastWhiteSpace();

	// Search the section for the label.
	while(!mIFS.eof() && mIFS.peek() != L'[')
	{
		vector<wstring> line = GetLineAsString();
		if(line[0].compare(label) == 0)
		{
			out = line[1];
			return IO_Success;
		}
		// There may be white space between labels.
		MovePastWhiteSpace();
	}
	return IO_LabelNotFound;
}

IO_Result LevelReader::GetIntFromSection(const wstring& section, const wstring& label, int& out)
{
	// Is there an open file?
	if(!mIFS.is_open())
	{
		// No. Try opening the file again.
		OpenLevelFile(mFileName);
		if(!mIFS.is_open())
			return IO_CantOpenFile;
	}

	// Search the file for the section.
	if(MoveToSection(section) == IO_SectionNotFound)
	{
		// The section wasn't found.
		return IO_SectionNotFound;
	}

	// There may be white space between the section header and the first label.
	MovePastWhiteSpace();

	// Search the section for the label.
	while(!mIFS.eof() && mIFS.peek() != L'[')
	{
		vector<wstring> line = GetLineAsString();
		if(line[0].compare(label) == 0)
		{
			StringTo(line[1], out);
			return IO_Success;
		}
		// There may be white space between labels.
		MovePastWhiteSpace();
	}
	return IO_LabelNotFound;
}

IO_Result LevelReader::GetLongFromSection(const wstring& section, const wstring& label, long& out)
{
	// Is there an open file?
	if(!mIFS.is_open())
	{
		// No. Try opening the file again.
		OpenLevelFile(mFileName);
		if(!mIFS.is_open())
			return IO_CantOpenFile;
	}

	// Search the file for the section.
	if(MoveToSection(section) == IO_SectionNotFound)
	{
		// The section wasn't found.
		return IO_SectionNotFound;
	}

	// There may be white space between the section header and the first label.
	MovePastWhiteSpace();

	// Search the section for the label.
	while(!mIFS.eof() && mIFS.peek() != L'[')
	{
		vector<wstring> line = GetLineAsString();
		if(line[0].compare(label) == 0)
		{
			StringTo(line[1], out);
			return IO_Success;
		}
		// There may be white space between labels.
		MovePastWhiteSpace();
	}
	return IO_LabelNotFound;
}

IO_Result LevelReader::GetVectorsFromSection(const wstring& section, vector<D3DXVECTOR3>& vectors)
{
	// Is there an open file?
	if(!mIFS.is_open())
	{
		// No. Try opening the file again.
		OpenLevelFile(mFileName);
		if(!mIFS.is_open())
			return IO_CantOpenFile;
	}

	// Search the file for the section.
	if(MoveToSection(section) == IO_SectionNotFound)
	{
		// The section wasn't found.
		return IO_SectionNotFound;
	}

	// There may be white space between the section header and the first vector.
	MovePastWhiteSpace();

	// Add each line in the section into the container as a vector.
	while(!mIFS.eof() && mIFS.peek() != L'[')
	{
		vectors.push_back(GetLineAsVector());
		// There may be white space between vectors.
		MovePastWhiteSpace();
	}
	return IO_Success;
}

// Non-member functions.
auto_ptr<LevelData> BuildLevelFromFile(wstring fileName)
{
	auto_ptr<LevelData> level(new LevelData);
	LevelReader reader(fileName);
	reader.GetStringFromSection(L"Details", L"LevelName", level->mLevelName);
	reader.GetStringFromSection(L"Details", L"Description", level->mDescription);
	reader.GetStringFromSection(L"Details", L"Author", level->mAuthor);
	reader.GetStringFromSection(L"Audio", L"Music", level->mMusicFilename);
	reader.GetLongFromSection(L"Audio", L"PlayStart", level->mPlayStart);
	
	// Create normal bricks at the positions specified in the level file.
	// The y offset adjusts the position of the bricks. I'm too lazy to change
	// the values in the file.
	static const float yOffset(8.0f);
	vector<D3DXVECTOR3> positions;
	reader.GetVectorsFromSection(L"Brick.Normal", positions);
	for(unsigned i = 0; i < positions.size(); i++)
	{
		Brick* ptrBrick = new NormalBrick(64, 32);
		D3DXVECTOR3 position = positions[i];
		position.y += yOffset;
		ptrBrick->SetPosition(position);
		level->mBricks.push_back(ptrBrick);
	}

	// Create titanium bricks at the positions specified in the level file.
	positions.clear();
	reader.GetVectorsFromSection(L"Brick.Titanium", positions);
	for(unsigned i = 0; i < positions.size(); i++)
	{
		Brick* ptrBrick = new TitaniumBrick;
		D3DXVECTOR3 position = positions[i];
		position.y += yOffset;
		ptrBrick->SetPosition(position);
		level->mBricks.push_back(ptrBrick);
	}

	// Create marble bricks at the positions specified in the level file.
	positions.clear();
	reader.GetVectorsFromSection(L"Brick.Marble", positions);
	for(unsigned i = 0; i < positions.size(); i++)
	{
		Brick* ptrBrick = new MarbleBrick;
		D3DXVECTOR3 position = positions[i];
		position.y += yOffset;
		ptrBrick->SetPosition(position);
		level->mBricks.push_back(ptrBrick);
	}

	return level;
}