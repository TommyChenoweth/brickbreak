#include "Background.hpp"
#include "DXUtil.hpp"
#include "Sprite.hpp"

// Constructors and Destructors
Background::Background(IDirect3DDevice9 *d3dDevice, ID3DXSprite *d3dSprite, wstring fileName, float bgWidth, float bgHeight, float windowWidth, float windowHeight)
: Sprite(d3dDevice, d3dSprite, fileName),
  mWidth(bgWidth),
  mHeight(bgHeight)
{
	SetPosition(D3DXVECTOR3(0.0f, 0.0f, 1.0f));
	mTilesPerRow = static_cast<unsigned>(windowWidth/mWidth) + 1;
	mTilesPerColumn = static_cast<unsigned>(windowHeight/mHeight) + 1;
}

Background::~Background()
{
}

void Background::Draw()
{
	static D3DXVECTOR3 currPos(0.0f, 0.0f, 0.0f);
	currPos = mPosition;
	for(unsigned row = 0; row < mTilesPerRow; row++)
	{
		currPos.y = mPosition.y + (row * mHeight);
		for(unsigned col = 0; col < mTilesPerColumn; col++)
		{
			currPos.x = mPosition.x + (col * mWidth);
			HRESULT drawSprite = mD3DSprite->Draw(
				mD3DTex,
				0,
				0,
				&currPos,
				D3DCOLOR_XRGB(255, 255, 255));
			switch(drawSprite)
			{
			case D3DERR_INVALIDCALL:
				ErrorBox(L"Can't draw sprite: Invalid parameter(s).");
				HR(drawSprite);
				break;
			case D3DXERR_INVALIDDATA:
				ErrorBox(L"Can't draw sprite: Data corrupt/incorrect.");
				HR(drawSprite);
				break;
			}
		}
	}
}