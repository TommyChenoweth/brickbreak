#pragma once

#include "Sprite.hpp"

class Score : public Sprite
{
private:
	float			mTexWidth;
	float			mTexHeight;
	float			mCharWidth;
	float			mCharHeight;
	unsigned long	mScore;
	unsigned		mMaxDigits;
	int*			mDigits;
	RECT*			mFramesForDigits;
	float			mTtlWidth;

	void GenDigitsFromScore();
	RECT GetFrameForDigit(unsigned digit);

public:
	// Constructors and Destructor
	Score(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring fileName, float textureWidth, float textureHeight, float charWidth, float charHeight);
	~Score();

	// Accessors and Mutators
	void SetScore(unsigned long score);
	long GetScore();
	void AddToScore(long numToAdd);

	void Draw();
};