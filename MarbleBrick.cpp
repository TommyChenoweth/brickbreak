#include "MarbleBrick.hpp"

float MarbleBrick::mDT = 0.0f;
IDirect3DTexture9* MarbleBrick::mCracksTex = 0;
const unsigned MarbleBrick::mCrackRows(8);
const unsigned MarbleBrick::mCrackCols(4);
const unsigned MarbleBrick::mTotalFrames(30);

void MarbleBrick::AnimateToNextFrame()
{
	static const unsigned framesPerSecond(90);
	mTimePassed += mDT;
	while(1.0/framesPerSecond <= mTimePassed && mCurrFrame < mFrame)
	{
		mTimePassed -= 1.0/framesPerSecond;
		mCurrFrame++;
	}
}

MarbleBrick::MarbleBrick()
: SpriteBrick<MarbleBrick>(64, 32),
  mTimesStruck(0),
  mStrikesToDestroy(4),
  mFrame(0),
  mCurrFrame(0),
  mTimePassed(0.0)
{
}
MarbleBrick::~MarbleBrick()
{
	
}

int MarbleBrick::Struck()
{
	mTimesStruck++;
	mFrame = static_cast<unsigned>((static_cast<float>(mTimesStruck) / static_cast<float>(mStrikesToDestroy - 1)) * static_cast<float>(mTotalFrames - 1) + 0.5f);
	PlaySFX();
	if(mTimesStruck == mStrikesToDestroy)
	{
		mDestroyed = true;
		return mPoints;
	}
	return mPoints/2;
}
void MarbleBrick::Reset()
{
	mDestroyed = false;
	mFrame = 0;
	mTimesStruck = 0;
	mCurrFrame = 0;
}
void MarbleBrick::Draw()
{
	HRESULT drawSprite = mD3DSprite->Draw(
		mD3DTex,
		0,
		0,
		&mPosition,
		D3DCOLOR_XRGB(255, 255, 255));
	switch(drawSprite)
	{
	case D3DERR_INVALIDCALL:
		ErrorBox(L"Can't draw sprite: Invalid parameter(s).");
		HR(drawSprite);
		break;
	case D3DXERR_INVALIDDATA:
		ErrorBox(L"Can't draw sprite: Data corrupt/incorrect.");
		HR(drawSprite);
		break;
	}

	if(mCurrFrame != mFrame)
		AnimateToNextFrame();

	static RECT r;
	r.left = static_cast<long>((mCurrFrame % mCrackCols) * GetBounds().GetWidth());
	r.top = static_cast<long>((mCurrFrame / mCrackCols) * GetBounds().GetHeight());
	r.right = static_cast<long>(r.left + GetBounds().GetWidth());
	r.bottom = static_cast<long>(r.top + GetBounds().GetHeight());

	mD3DSprite->Draw(
		mCracksTex,
		&r,
		0,
		&mPosition,
		D3DCOLOR_XRGB(255, 255, 255));
}

void MarbleBrick::LoadTexture(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring filename)
{
	SharedSprite<MarbleBrick>::LoadTexture(d3dDevice, d3dSprite, filename);
	HRESULT createTex = D3DXCreateTextureFromFile(d3dDevice, L"Graphics/brick_breaking.dds", &mCracksTex);
	switch(createTex)
	{
	case D3DERR_NOTAVAILABLE:
		HR(createTex);
		break;
	case D3DERR_OUTOFVIDEOMEMORY:
		ErrorBox(L"Can't create texture: Direct3D does not have enough display memory to perform the operation");
		HR(createTex);
		break;
	case D3DERR_INVALIDCALL:
		HR(createTex);
		break;
	case D3DXERR_INVALIDDATA:
		ErrorBox(L"Can't create texture: Invalid data. Check filenames.");
		HR(createTex);
		break;
	case E_OUTOFMEMORY:
		ErrorBox(L"Can't create texture: Direct3D could not allocate sufficient memory to complete the call.");
		HR(createTex);
		break;
	}
}

void MarbleBrick::ReleaseTexture()
{
	SharedSprite<MarbleBrick>::ReleaseTexture();
	ReleaseCOM(mCracksTex);
}

void MarbleBrick::UpdateDT(float dt)
{
	mDT = dt;
}