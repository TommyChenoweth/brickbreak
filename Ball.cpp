#include <d3d9.h>
#include <D3dx9core.h>
#include <string>
#include <math.h>
#include "Ball.hpp"
#include "DXUtil.hpp"
#include "BoundingSphere.hpp"

using namespace std;

// Constructors and Destructor
Ball::Ball(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring fileName)
: Sprite(d3dDevice, d3dSprite, fileName),
  mOrientation(0.5f, 1.0f, 0.0f),
  mCenter(0.5f, 0.5f, 0.0f),
  mBounds(0),
  mSpeed(800),
  mLocked(false),
  mColliding(false)
{
	float radius(10.0f);
	mBounds = new BoundingSphere(mPosition + D3DXVECTOR3(radius, radius, 0), radius);
	D3DXVec3Normalize(&mOrientation, &(D3DXVECTOR3(mOrientation)));
}
Ball::~Ball()
{
	DeletePtr(mBounds);
}

// Accessors and Mutators
// mPosition
void Ball::SetPosition(D3DXVECTOR3 position)
{
	if(IsLocked())
	{
		mPosition = position;
		mBounds->BuildSphereFromTopLeft(mPosition);
	}
	else
	{
		mPosition = position;
		mBounds->BuildSphereFromTopLeft(mPosition);
	}
}
// mOrientation
D3DXVECTOR3 Ball::GetOrientation()
{
	return mOrientation;
}
void Ball::SetOrientation(D3DXVECTOR3 orientation)
{
	mOrientation = orientation;
}
// mBoundingSphere
BoundingSphere Ball::GetBounds()
{
	return (*mBounds);
}
// mSpeed
unsigned Ball::GetSpeed()
{
	return mSpeed;
}
void Ball::SetSpeed(unsigned speed)
{
	mSpeed = speed;
}
// mLocked
void Ball::Lock()
{
	mLocked = true;
}
void Ball::Unlock()
{
	mLocked = false;
}
bool Ball::IsLocked()
{
	return mLocked;
}
// mColliding
bool Ball::AlreadyCollided()
{
	return mColliding;
}
void Ball::Collide()
{
	mColliding = true;
}
void Ball::DoneColliding()
{
	mColliding = false;
}

void Ball::Draw()
{
	HRESULT drawSprite = mD3DSprite->Draw(
		mD3DTex,
		0,
		&mCenter,
		&mPosition,
		D3DCOLOR_XRGB(255, 255, 255));
	switch(drawSprite)
	{
	case D3DERR_INVALIDCALL:
		ErrorBox(L"Can't draw sprite: Invalid parameter(s).");
		HR(drawSprite);
		break;
	case D3DXERR_INVALIDDATA:
		ErrorBox(L"Can't draw sprite: Data corrupt/incorrect.");
		HR(drawSprite);
		break;
	}
}

void BallWallCollision(Ball* ball, const RECT& walls)
{
	static BoundingSphere bSphere(D3DXVECTOR3(0.0f, 0.0f, 0.0f), 1);
	bSphere = ball->GetBounds();

	// Check for collision with the top wall.
	static float ballTopSide(0.0f);
	ballTopSide = bSphere.GetCenter().y - bSphere.GetRadius();
	static bool ballCollideTopWall(false);
	ballCollideTopWall = (ballTopSide <= walls.top);

	if(ballCollideTopWall)
	{
		D3DXVECTOR3 ballDir = ball->GetOrientation();
		ballDir.y = fabs(ballDir.y);
		ball->SetOrientation(ballDir);
		return;
	}

	// Check for collision with the bottom wall.
	//static float ballBottomSide(0.0f);
	//ballBottomSide = bSphere.GetCenter().y + bSphere.GetRadius();
	//static bool ballCollideBottomWall(false);
	//ballCollideBottomWall = (ballBottomSide >= walls.bottom);

	//if(ballCollideBottomWall)
	//{
	//	D3DXVECTOR3 ballDir = ball->GetOrientation();
	//	ballDir.y = -fabs(ballDir.y);
	//	ball->SetOrientation(ballDir);
	//	return;
	//}

	// Check for collision with the left wall.
	static float ballLeftSide(0.0f);
	ballLeftSide = bSphere.GetCenter().x - bSphere.GetRadius();
	static bool ballCollideLeftWall(false);
	ballCollideLeftWall = (ballLeftSide <= walls.left);

	if(ballCollideLeftWall)
	{
		D3DXVECTOR3 ballDir = ball->GetOrientation();
		ballDir.x = fabs(ballDir.x);
		ball->SetOrientation(ballDir);
		return;
	}

	// Check for collision with the right wall.
	static float ballRightSide(0.0f);
	ballRightSide = bSphere.GetCenter().x + bSphere.GetRadius();
	static bool ballCollideRightWall(false);
	ballCollideRightWall = (ballRightSide >= walls.right);

	if(ballCollideRightWall)
	{
		D3DXVECTOR3 ballDir = ball->GetOrientation();
		ballDir.x = -fabs(ballDir.x);
		ball->SetOrientation(ballDir);
		return;
	}
}