#include "SpriteBrick.hpp"
#include "TitaniumBrick.hpp"

// Constructors and Destructor
TitaniumBrick::TitaniumBrick()
: SpriteBrick<TitaniumBrick>(64, 32)
{
}
TitaniumBrick::~TitaniumBrick()
{
}

int TitaniumBrick::Struck()
{
	PlaySFX();
	return 0;
}