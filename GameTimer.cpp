#include <d3d9.h>
#include <d3dx9core.h>
#include <string>
#include "boost\format.hpp"
#include "GameTimer.hpp"
#include "DXUtil.hpp"

using namespace std;
using namespace boost;

GameTimer::GameTimer(ID3DXFont* d3dFont, ID3DXSprite* d3dSprite)
: mD3DFont(d3dFont),
  mD3DSprite(d3dSprite),
  mPauseCount(0),
  mPaused(false),
  mResumed(false),
  mCntsPrevFrame(0),
  mCntsCurrFrame(0),
  mSecondsPerCnt(0),
  dt(0),
  mFPS(0),
  mMSPerFrame(0),
  mTextDisplay(256, ' ')
{
	mTextColor = D3DCOLOR_XRGB(0, 0, 0);

	__int64 cntsPerSecond(0);
	QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&cntsPerSecond));
	mSecondsPerCnt = 1.0f/static_cast<float>(cntsPerSecond);

	// This ensures that dt is accurately calculated the first time UpdateTimer is called.
	// Without these assignments, the previous frame time would be zero, so dt would be
	// insanely high.
	mCntsCurrFrame = GetCounts();
	mCntsPrevFrame = mCntsCurrFrame;
}

__int64 GameTimer::GetCounts()
{
	__int64 cnts(0);
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&cnts));
	return cnts;
}

float GameTimer::GetDeltaT()
{
	return dt;
}
void GameTimer::Pause()
{
	mPauseCount++;
	if(0 < mPauseCount)
	{
		mPaused = true;
	}
	wprintf(L"PauseCount: %i\n", mPauseCount);
}
void GameTimer::Resume()
{
	mPauseCount--;
	if(mPauseCount <= 0)
	{
		mPaused = false;
		mResumed = true;
	}
	wprintf(L"PauseCount: %i\n", mPauseCount);
}
bool GameTimer::IsPaused()
{
	return mPaused;
}

void GameTimer::SetTextColor(D3DCOLOR textColor)
{
	mTextColor = textColor;
}

void GameTimer::UpdateTimer()
{
	static float timeElapsed(0);
	static int numFrames(0);

	if(mResumed)
	{
		mCntsCurrFrame = GetCounts();
		mCntsPrevFrame = mCntsCurrFrame;
		mResumed = false;
	}
	else
	{
		mCntsPrevFrame = mCntsCurrFrame;
		mCntsCurrFrame = GetCounts();
	}

	dt = (mCntsCurrFrame - mCntsPrevFrame) * mSecondsPerCnt;
	timeElapsed += dt;

	numFrames++;

	// Compute FPS and MSPF every second.
	if(1.0f <= timeElapsed)
	{
		mFPS = static_cast<float>(numFrames);
		numFrames = 0;

		mMSPerFrame = 1000.0f/mFPS;

		mTextDisplay = str( wformat(L"Frames Per Second = %s\nMilliseconds Per Frame = %f") % mFPS % mMSPerFrame );
		
		timeElapsed = 0;
		numFrames = 0;
	}
}

void GameTimer::DrawTimerText()
{
	#if defined(DEBUG) | defined(_DEBUG)
		static RECT r = {5, 5, 0, 0};
		mD3DFont->DrawText(mD3DSprite, mTextDisplay.c_str(), -1, &r, DT_NOCLIP, mTextColor);
	#endif
}