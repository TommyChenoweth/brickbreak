#pragma once

#include <d3dx9core.h>

class BoundingSphere
{
private:
	D3DXVECTOR3 mCenter;
	float		mRadius;

public:
	// Constructors and Destructor
	BoundingSphere(D3DXVECTOR3 center, float radius);
	~BoundingSphere();

	// Accessors and Mutators
	D3DXVECTOR3 GetCenter() const;
	void SetCenter(D3DXVECTOR3 center);

	float GetRadius() const;
	void SetRadius(float radius);

	void BuildSphereFromTopLeft(D3DXVECTOR3 topLeft);
};

bool IsPtInSphere(const D3DXVECTOR3& pt, const BoundingSphere& bSphere);