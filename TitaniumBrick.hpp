#pragma once

#include "SpriteBrick.hpp"

class TitaniumBrick : public SpriteBrick<TitaniumBrick>
{
private:
public:
	// Constructors and Destructor
	TitaniumBrick();
	~TitaniumBrick();

	int Struck();
};