#pragma once

#include <d3d9.h>
#include "Sprite.hpp"
#include <string>

using namespace std;

class Lives : public Sprite
{
private:
	unsigned	mStartingLives;
	unsigned	mCurrLives;
	wstring		mFilename;

public:
	// Constructors and Destructor
	Lives(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, unsigned numLives);
	~Lives();

	// Accessors and Mutators
	// mCurrLives
	void AddLife();
	void RemoveLife();
	void ResetLives();
	unsigned GetNumLives();

	void Draw();
};