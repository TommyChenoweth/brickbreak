#include <d3dx9core.h>
#include "BoundingBox.hpp"

// Constructors and Destructor
BoundingBox::BoundingBox(float left, float top, float width, float height)
: mTopLeft(left, top, 0.0f),
  mWidth(1.0f),
  mHeight(1.0f),
  mCenter(left+(0.5f*mWidth), top+(0.5f*mHeight), 0.0f),
  mBottomRight(left+mWidth, top+mHeight, 0.0f)
{
	SetWidth(width);
	SetHeight(height);
	BuildBoxFromTopLeft(mTopLeft);
}
BoundingBox::~BoundingBox()
{
}

// Accessors and Mutators
D3DXVECTOR3 BoundingBox::GetCenter() const
{
	return mCenter;
}
float BoundingBox::GetWidth() const
{
	return mWidth;
}
void BoundingBox::SetWidth(float width)
{
	0 < width ? mWidth = width : 0;
}
float BoundingBox::GetHeight() const
{
	return mHeight;
}
void BoundingBox::SetHeight(float height)
{
	0 < height ? mHeight = height : 0;
}

float BoundingBox::GetLeftSide()  const
{
	return mTopLeft.x;
}

float BoundingBox::GetRightSide()  const
{
	return mBottomRight.x;
}

float BoundingBox::GetTopSide()  const
{
	return mTopLeft.y;
}

float BoundingBox::GetBottomSide()  const
{
	return mBottomRight.y;
}

void BoundingBox::BuildBoxFromTopLeft(D3DXVECTOR3 topLeft)
{
	mTopLeft = topLeft;
	mCenter.x = mTopLeft.x + (0.5f * mWidth);
	mCenter.y = mTopLeft.y + (0.5f * mHeight);
	mBottomRight.x = mTopLeft.x + mWidth;
	mBottomRight.y = mTopLeft.y + mHeight;
}

// Non-Member Functions
bool IsPtInBox(const D3DXVECTOR3& pt, const BoundingBox& box)
{
	bool ptIsBetweenLeftAndRightSides = box.GetLeftSide() <= pt.x && pt.x <= box.GetRightSide();
	bool ptIsBetweenTopAndBottomSides = box.GetTopSide() <= pt.y && pt.y <= box.GetBottomSide();
	if(ptIsBetweenLeftAndRightSides && ptIsBetweenTopAndBottomSides)
		return true;
	return false;
}