#include "Ball.hpp"
#include "BrickBreak.hpp"
#include "boost/random/mersenne_twister.hpp"
#include <irrKlang.h>
#include "Paddle.hpp"
#include "PowerMod.hpp"
#include <time.h>

using namespace irrklang;

// File scope variables used by the "Action" functions.
ISound*	g_Music;
Score*	g_Score;

void BrickBreak::PassVarsToPMActions()
{
	g_Score = mScore;
	g_Music = mMusic;
}

void FastBall(Ball* ball, Paddle* paddle);
void SlowBall(Ball* ball, Paddle* paddle);
void GrowPaddle(Ball* ball, Paddle* paddle);
void ShrinkPaddle(Ball* ball, Paddle* paddle);

void (*Action[])(Ball*, Paddle*) = {&SlowBall, &GrowPaddle, &FastBall, &ShrinkPaddle};
const int ACTIONSIZE = 4;

PowerMod GetRandomPowerMod()
{
	static boost::mt19937 rand;
	static bool doneOnce(false);
	if(!doneOnce)
	{
		rand.seed(static_cast<boost::uint32_t>(time(0)));
		doneOnce = true;
	}

	int randNum = rand() % ACTIONSIZE;

	PowerMod mod;
	mod.Action = Action[randNum];
	mod.SetFrame(randNum);
	return mod;
}

void FastBall(Ball* ball, Paddle* paddle)
{
	static const float maxSpeed = 1200.0f;
	static const float increment = 200.0f;
	static const float musicPlaybackIncrement = 0.05f;

	g_Score->AddToScore(333);

	float currSpeed(static_cast<float>(ball->GetSpeed()));
	if(currSpeed < maxSpeed)
	{
		g_Music->setPlaybackSpeed(g_Music->getPlaybackSpeed()+musicPlaybackIncrement);
		currSpeed += increment;
		if(maxSpeed < currSpeed)
			currSpeed = maxSpeed;
		ball->SetSpeed(static_cast<unsigned>(currSpeed));
	}
}
void SlowBall(Ball* ball, Paddle* paddle)
{
	static const float minSpeed = 600.0f;
	static const float increment = -200.0f;
	static const float musicPlaybackIncrement = -0.05f;

	g_Score->AddToScore(333);

	float currSpeed(static_cast<float>(ball->GetSpeed()));
	if(minSpeed < currSpeed)
	{
		g_Music->setPlaybackSpeed(g_Music->getPlaybackSpeed()+musicPlaybackIncrement);
		currSpeed += increment;
		if(currSpeed < minSpeed)
			currSpeed = minSpeed;
		ball->SetSpeed(static_cast<unsigned>(currSpeed));
	}
}

void GrowPaddle(Ball* ball, Paddle* paddle)
{
	static const unsigned increment = 30;
	static const unsigned maxSegments = paddle->GetNumSegments() + increment*2;

	g_Score->AddToScore(333);

	static unsigned segments(0);
	segments = paddle->GetNumSegments();

	segments += increment;

	if(maxSegments < segments)
		segments = maxSegments;

	paddle->SetNumSegments(segments);
}

void ShrinkPaddle(Ball* ball, Paddle* paddle)
{
	static const int increment = -30;
	static const unsigned minSegments = paddle->GetNumSegments() + increment*2;

	g_Score->AddToScore(333);

	static unsigned segments(0);
	segments = paddle->GetNumSegments();

	segments += increment;

	if(segments < minSegments)
		segments = minSegments;

	paddle->SetNumSegments(segments);
}