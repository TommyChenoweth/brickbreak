===============================================================================
DESCRIPTION
===============================================================================

This is a primitive Brick Breaker clone that I wrote in 2011. You can download
the compiled game from the "Downloads" link on the left.

===============================================================================
FEATURES
===============================================================================

* Multiple brick types.
* Power-ups to increase paddle size and slow the ball.
* Power-downs to decrease paddle size and speed up the ball.
* Music that changes tempo based on the speed of the ball.
* Sound effects for brick-ball collision and power-ups.

===============================================================================
VIDEOS
===============================================================================

https://www.youtube.com/watch?v=XnyPppOMlG8
Watch a video of the game here: