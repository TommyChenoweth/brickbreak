#pragma once

#include <d3dx9.h>
#include <dxerr.h>
#include <string>

// Enable extra D3D debugging in debug builds if using the debug DirectX runtime.  
// This makes D3D objects work well in the debugger watch window, but slows down 
// performance slightly.
#if defined(DEBUG) | defined(_DEBUG)
#ifndef D3D_DEBUG_INFO
#define D3D_DEBUG_INFO
#endif
#endif

//===============================================================
// Clean up
#define ReleaseCOM(x) { if(x){ x->Release();x = 0; } }
#define DeletePtr(x) { if(x){ delete x;x = 0;} }
#define DeleteAPtr(x) { if(x){ delete [] x; x = 0;} }

//===============================================================
// Debug
#if defined(DEBUG) | defined(_DEBUG)
	#ifndef HR
	#define HR(x)												\
	{															\
		HRESULT hr = x;											\
		if(FAILED(hr))											\
		{														\
			DXTrace(__FILE__, (DWORD)__LINE__, hr, L#x, TRUE);	\
		}														\
	}
	#endif

#else
	#ifndef HR
	#define HR(x) x;
	#endif
#endif 

//===============================================================
// Error Output
void ErrorBox(std::wstring message);