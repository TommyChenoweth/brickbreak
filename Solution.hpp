#pragma once

class Solution
{
private:
	float	mSolution;
	bool	mReal;

public:
	// Constructors and Destructor
	Solution(float solution);
	Solution();
	~Solution();

	// Accessors and Mutators
	// mSolution
	float GetSolution();
	void SetSolution(float solution);
	// mReal
	bool IsReal();
	void SetReal();
	void SetImaginary();
};