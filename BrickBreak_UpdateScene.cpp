#include <D3dx9core.h>
#include "BrickBreak.hpp"
#include "Ball.hpp"
#include <cmath>
#include <boost/random/mersenne_twister.hpp>
#include "Paddle.hpp"
#include "Keyboard.hpp"
#include "BoundsUtil.hpp"
#include "Brick.hpp"
#include "MarbleBrick.hpp"

struct CollisionData
{
	CollisionData()
		: mBrick(0),
		  mCollisionPt(0.0f, 0.0f, 0.0f),
		  mRestingPt(0.0f, 0.0f, 0.0f)
	{}
	Brick* mBrick;
	D3DXVECTOR3 mCollisionPt;
	D3DXVECTOR3 mRestingPt;
};

bool BallOutOfBounds(Ball* ball, const RECT& walls);
bool BallBrickCollision(Ball* ball, Brick* brick, float dt, CollisionData& cd);
void MoveBallToCollision(Ball* ball, CollisionData& data, float dt);
void BallMove(Ball* ball, float dt);
bool BallPaddleCollision(Ball* ball, Paddle* paddle, float dt);
void MovePaddleWithMouse(Paddle* paddle, Keyboard* input);
bool ShouldCreatePowerMod();
void MovePowerMods(vector<PowerMod>& powerMods, float dt);
void PowerModWallCollision(vector<PowerMod>& powerMods, const RECT& walls);
void PowerModPaddleCollision(vector<PowerMod>& powerMods, Paddle* paddle, Ball* ball);

void BrickBreak::UpdateScene(float dt)
{
	// Marble bricks need the time difference to play their animations.
	MarbleBrick::UpdateDT(dt);

	const static RECT playField = {0, 72, mWindowWidth, mWindowHeight};
	if(mInput->IsMouseActive())
	{
		MovePaddleWithMouse(mPaddle, mInput);
		PaddleWallCollision(mPaddle, playField);
	}
	else if(mInput->IsKeyDown(KEY_LEFTARROW))
	{
		PaddleMoveLeft(mPaddle, dt);
		PaddleWallCollision(mPaddle, playField);
	}
	else if(mInput->IsKeyDown(KEY_RIGHTARROW))
	{
		PaddleMoveRight(mPaddle, dt);
		PaddleWallCollision(mPaddle, playField);
	}

	// Find the nearest brick collision point if it exists.
	CollisionData nearestCollision;
	for(unsigned i = 0; i < mLevelData->mBricks.size(); i++)
	{
		if(mLevelData->mBricks[i]->IsDestroyed())
			continue;

		CollisionData currCollision;
		if(BallBrickCollision(mMainBall, mLevelData->mBricks[i], dt, currCollision))
		{
			if(D3DXVec3Length(&(mMainBall->GetBounds().GetCenter() - currCollision.mRestingPt)) < D3DXVec3Length(&(mMainBall->GetBounds().GetCenter() - nearestCollision.mRestingPt)))
			{
				nearestCollision = currCollision;
			}
		}
	}

	if(nearestCollision.mBrick)
	{
		MoveBallToCollision(mMainBall, nearestCollision, dt);
		unsigned points = nearestCollision.mBrick->Struck();
		mScore->AddToScore(points);
		if(nearestCollision.mBrick->IsDestroyed())
		{
			if(ShouldCreatePowerMod())
			{
				PowerMod mod = GetRandomPowerMod();
				mod.SetPosition(nearestCollision.mBrick->GetPosition());
				mPowerMods.push_back(mod);
			}
		}
	}
	else
	{
		if(!BallPaddleCollision(mMainBall, mPaddle, dt))
		{
			BallMove(mMainBall, dt);
		}
	}

	BallWallCollision(mMainBall, playField);

	if(BallOutOfBounds(mMainBall, playField))
		LifeLost();

	MovePowerMods(mPowerMods, dt);
	PowerModWallCollision(mPowerMods, playField);

	if(mInput->IsKeyDown(MOUSE_LEFTBUTTON))
		mPaddle->ReleaseLockedBalls();

	PowerModPaddleCollision(mPowerMods, mPaddle, mMainBall);
}
bool BallOutOfBounds(Ball* ball, const RECT& walls)
{
	if(ball->GetPosition().y >= walls.bottom)
		return true;
	return false;
}
bool BallBrickCollision(Ball* ball, Brick* brick, float dt, CollisionData& cd)
{
	if(ball->IsLocked())
		return false;

	static D3DXVECTOR3 newCenterPt(0.0f, 0.0f, 0.0f);
	static D3DXVECTOR3 ptOfCollision(0.0f, 0.0f, 0.0f);

	static D3DXVECTOR3 oldPos(0.0f, 0.0f, 0.0f);
	oldPos = ball->GetPosition();
	BallMove(ball, dt);
	static BoundingSphere nextBounds(ball->GetPosition(), ball->GetBounds().GetRadius());
	nextBounds.SetRadius(ball->GetBounds().GetRadius());
	nextBounds.BuildSphereFromTopLeft(ball->GetPosition());
	ball->SetPosition(oldPos);

	if(Collision_Sweep_CircleSquare(ball->GetBounds(), nextBounds, brick->GetBounds(), ptOfCollision, newCenterPt))
	{
		cd.mBrick = brick;
		cd.mCollisionPt = ptOfCollision;
		cd.mRestingPt = newCenterPt;
		return true;
	}

	return false;
}

void MoveBallToCollision(Ball* ball, CollisionData& data, float dt)
{
	if(ball->IsLocked())
		return;

	D3DXVECTOR3 newPos(0.0f, 0.0f, 0.0f);
	newPos = data.mRestingPt;
	newPos.x -= ball->GetBounds().GetRadius();
	newPos.y -= ball->GetBounds().GetRadius();
	ball->SetPosition(newPos);

	D3DXVECTOR3 collisionVector(0.0f, 0.0f, 0.0f);
	collisionVector = data.mCollisionPt - ball->GetBounds().GetCenter();
	D3DXVec3Normalize(&collisionVector, &collisionVector);
	D3DXVECTOR3 currBallDir = ball->GetOrientation();
	D3DXVECTOR3 newBallDir = -2 * D3DXVec3Dot(&currBallDir, &collisionVector) * collisionVector + currBallDir;
	ball->SetOrientation(newBallDir);

	// Move the ball outside of the brick.
	int count(0);
	int side(SphereIntersectsBox(ball->GetBounds(), data.mBrick->GetBounds(), D3DXVECTOR3(0.0f, 0.0f, 0.0f)));
	while(side != 0)
	{
		static const float increment(0.01f);
		switch(side)
		{
		case 1: // Top
			ball->SetPosition(ball->GetPosition() + D3DXVECTOR3(0.0f, -increment, 0.0f));
			//wprintf(L"Move ball up\n");
			break;
		case 2: // Bottom
			ball->SetPosition(ball->GetPosition() + D3DXVECTOR3(0.0f, increment, 0.0f));
			//wprintf(L"Move ball down\n");
			break;
		case 3: // Left
			ball->SetPosition(ball->GetPosition() + D3DXVECTOR3(-increment, 0.0f, 0.0f));
			//wprintf(L"Move ball left\n");
			break;
		case 4: // Right
			ball->SetPosition(ball->GetPosition() + D3DXVECTOR3(increment, 0.0f, 0.0f));
			//wprintf(L"Move ball right\n");
			break;
		case 5: // Corner
			static D3DXVECTOR3 vecToBall(0.0f, 0.0f, 0.0f);
			vecToBall = ball->GetBounds().GetCenter() - data.mBrick->GetBounds().GetCenter();
			D3DXVec3Normalize(&vecToBall, &vecToBall);
			ball->SetPosition(ball->GetPosition() + (vecToBall * 0.5f));
			//wprintf(L"Move ball out of corner\n");
			break;
		}
		count++;
		side = SphereIntersectsBox(ball->GetBounds(), data.mBrick->GetBounds(), D3DXVECTOR3(0.0f, 0.0f, 0.0f));
	}
	wprintf(L"Iterations: %i\n", count);
	wprintf(L"\n|*------------------------------------------>\n");
}

void BallMove(Ball* ball, float dt)
{
	if(ball->IsLocked())
		return;

	static D3DXVECTOR3 ballDir(0.0f, 0.0f, 0.0f);
	ballDir = ball->GetOrientation();
	static D3DXVECTOR3 ballPos(0.0f, 0.0f, 0.0f);
	ballPos = ball->GetPosition();
	ballPos += ballDir * static_cast<float>(ball->GetSpeed()) * dt;
	ball->SetPosition(ballPos);
}
bool BallPaddleCollision(Ball* ball, Paddle* paddle, float dt)
{
	if(ball->IsLocked())
		return false;;

	static D3DXVECTOR3 newCenterPt(0.0f, 0.0f, 0.0f);
	static D3DXVECTOR3 ptOfCollision(0.0f, 0.0f, 0.0f);

	static D3DXVECTOR3 oldPos(0.0f, 0.0f, 0.0f);
	oldPos = ball->GetPosition();
	BallMove(ball, dt);
	static BoundingSphere nextBounds(ball->GetPosition(), ball->GetBounds().GetRadius());
	nextBounds.SetRadius(ball->GetBounds().GetRadius());
	nextBounds.BuildSphereFromTopLeft(ball->GetPosition());
	ball->SetPosition(oldPos);

	bool collision = Collision_Sweep_CircleSquare(ball->GetBounds(), nextBounds, paddle->GetBounds(), ptOfCollision, newCenterPt);
	if(!collision)
	{
		ball->DoneColliding();
		return false;
	}

	if(ball->AlreadyCollided())
		return false;

	wprintf(L"Paddle Collision\n");
	wprintf(L"\n|*------------------------------------------>\n");

	ball->Collide();

	ball->SetPosition(D3DXVECTOR3(newCenterPt.x - ball->GetBounds().GetRadius(), newCenterPt.y - ball->GetBounds().GetRadius(), 0.0f));

	// Change the orientation of the ball.
	static D3DXVECTOR3 ballDir(0.0f, 0.0f, 0.0f);
	ballDir = ball->GetBounds().GetCenter() - paddle->GetBounds().GetCenter();
	static D3DXVECTOR3 ballDirNorm(0.0f, 0.0f, 0.0f);
	D3DXVec3Normalize(&ballDirNorm, &ballDir);
	static const D3DXVECTOR3 YAXIS(0.0f, -1.0f, 0.0f);
	// Compute the angle of deflection. Note that we must use two normal vectors
	// for this because the dot product of two vectors is scaled by their magnitudes.
	float theta = acos(D3DXVec3Dot(&YAXIS, & ballDirNorm));
	// Convert the angle from radians to degrees.
	theta *= 180/D3DX_PI;
	wprintf(L"Theta: %f\n", theta);
	if(theta < 90)
	{
		static const D3DXVECTOR3 DIR_WEIGHT(0.0f, -120.0f, 0.0f);
		// Angles greater than than 10 degrees only recieve a fraction of the weight, and angles
		// less than 10 degrees recieve the full weight.
		float scalarWeight = (10/(theta+1));
		scalarWeight = 1 < scalarWeight ? 1 :scalarWeight;
		wprintf(L"ScalarWeight: %f\n", scalarWeight);
		ballDir += DIR_WEIGHT * scalarWeight;
		D3DXVec3Normalize(&ballDirNorm, & ballDir);
	}
	ball->SetOrientation(ballDirNorm);

	return true;
}

void MovePaddleWithMouse(Paddle* paddle, Keyboard* input)
{
	// Get the position of both the mouse and paddle.
	static D3DXVECTOR3 paddlePos(0.0f, 0.0f, 0.0f);
	paddlePos = paddle->GetPosition();
	static D3DXVECTOR3 mousePos(0.0f, 0.0f, 0.0f);
	mousePos = input->GetMousePosition();
	static float paddleWidth(1.0f);
	paddleWidth = paddle->GetBounds().GetWidth();

	// Move the paddle with the mouse.
	paddlePos.x = mousePos.x - (0.5f * paddleWidth);
	paddle->SetPosition(paddlePos);
}

bool ShouldCreatePowerMod()
{
	static boost::mt19937 rand;
	static bool doneOnce(false);
	if(!doneOnce)
	{
		rand.seed(static_cast<boost::uint32_t>(time(0)));
		doneOnce = true;
	}

	int randNum = rand() % 20;
	if(randNum < 5)
	{
		return true;
	}
	return false;
}

void MovePowerMods(vector<PowerMod>& powerMods, float dt)
{
	static const float speed(200.0f);

	for(unsigned i = 0; i < powerMods.size(); i++)
	{
		static D3DXVECTOR3 position(0.0f, 0.0f, 0.0f);
		position = powerMods[i].GetPosition();
		position.y += dt * speed;
		powerMods[i].SetPosition(position);
	}
}

void PowerModWallCollision(vector<PowerMod>& powerMods, const RECT& walls)
{
	static vector<PowerMod>::iterator it;
	it = powerMods.begin();

	while(it != powerMods.end())
	{
		if(PowerModWallCollision((*it), walls))
		{
			powerMods.erase(it);
			// The iterator is invalidated after erasing an item.
			it = powerMods.begin();
			continue;
		}
		it++;
	}
}

void PowerModPaddleCollision(vector<PowerMod>& powerMods, Paddle* paddle, Ball* ball)
{
	static vector<PowerMod>::iterator it;
	it = powerMods.begin();

	while(it != powerMods.end())
	{
		if(BoxIntersectsBox(it->GetBounds(), paddle->GetBounds()))
		{
			if(it->Action)
				it->Action(ball, paddle);

			it->PlaySFX();

			powerMods.erase(it);
			it = powerMods.begin();
			continue;
		}
		it++;
	}
}