#pragma once

#include "BoundingBox.hpp"
#include <d3dx9core.h>

class Brick
{
protected:
	unsigned		mPoints;
	bool			mDestroyed;

public:
	// Constructors and Destructor
	Brick();
	virtual ~Brick();

	// Accessors and Mutators
	// mBounds
	virtual BoundingBox GetBounds() = 0;
	// mPosition
	virtual D3DXVECTOR3 GetPosition() = 0;
	virtual void SetPosition(D3DXVECTOR3 position) = 0;
	// mDestroyed
	bool IsDestroyed();
	virtual int Struck();
	virtual void Reset();

	virtual void Draw() = 0;
	
};