#pragma once

#include <d3dx9core.h>

class BoundingBox
{
private:
	D3DXVECTOR3	mTopLeft;
	D3DXVECTOR3	mBottomRight;
	D3DXVECTOR3 mCenter;
	float		mWidth;
	float		mHeight;

public:
	// Constructors and Destructor
	BoundingBox(float left, float top, float width, float height);
	~BoundingBox();

	// Accessors and Mutators
	D3DXVECTOR3 GetCenter() const;
	float GetWidth() const;
	void SetWidth(float width);
	float GetHeight() const;
	void SetHeight(float height);
	float GetLeftSide() const;
	float GetRightSide() const;
	float GetTopSide() const;
	float GetBottomSide() const;

	void BuildBoxFromTopLeft(D3DXVECTOR3 topLeft);
};

bool IsPtInBox(const D3DXVECTOR3& pt, const BoundingBox& box);