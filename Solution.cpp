#include "Solution.hpp"

// Constructors and Destructor
Solution::Solution(float solution)
: mSolution(solution),
  mReal(true)
{
}
Solution::Solution()
: mSolution(0.0f),
  mReal(true)
{
}
Solution::~Solution()
{
}

// Accessors and Mutators
// mSolution
float Solution::GetSolution()
{
	return mSolution;
}
void Solution::SetSolution(float solution)
{
	mSolution = solution;
}
// mReal
bool Solution::IsReal()
{
	return mReal;
}
void Solution::SetReal()
{
	mReal = true;
}
void Solution::SetImaginary()
{
	mReal = false;
}