#pragma once

#include <d3dx9core.h>
#include <vector>

class BoundingSphere;
class BoundingBox;

using namespace std;

bool Collision_Sweep_CircleSquare(const BoundingSphere& bCircleBegin, const BoundingSphere& bCircleEnd, const BoundingBox& bBox, D3DXVECTOR3& outIntersectPt, D3DXVECTOR3& outRestingPt);
bool Collision_Static_LineSegmentSquare(const D3DXVECTOR3& lineSegBegin, const D3DXVECTOR3& lineSegEnd, const BoundingBox& box, vector<D3DXVECTOR3>& outIntersectionPts);
bool SphereIntersectsSphere(const BoundingSphere& bSphereA, const BoundingSphere& bSphereB);
int SphereIntersectsBox(const BoundingSphere& bSphere, const BoundingBox& bBox, const D3DXVECTOR3& direction);
bool BoxIntersectsBox(const BoundingBox& bBoxA, const BoundingBox& bBoxB);