#pragma once

#include "BoundingBox.hpp"
#include "Brick.hpp"
#include "SharedSound.hpp"
#include "SharedSprite.hpp"

template <typename T>
class SpriteBrick : public SharedSprite<T>, public SharedSound<T>, public Brick
{
private:
	BoundingBox mBounds;
public:
	// Constructors and Destructor
	SpriteBrick(unsigned width, unsigned height);
	virtual ~SpriteBrick();

	// Accessors and Mutators
	// mBounds
	BoundingBox GetBounds();
	// mPosition
	void SetPosition(D3DXVECTOR3 position);
	D3DXVECTOR3 GetPosition(){return SharedSprite<T>::GetPosition();};

	virtual void Draw();
};

// Constructors and Destructor
template <typename T>
SpriteBrick<T>::SpriteBrick(unsigned width, unsigned height)
: SharedSprite<T>(),
  Brick(),
  mBounds(0.0f, 0.0f, static_cast<float>(width), static_cast<float>(height))
{
}
template <typename T>
SpriteBrick<T>::~SpriteBrick()
{
}

// Accessors and Mutators
// mBounds
template <typename T>
BoundingBox SpriteBrick<T>::GetBounds()
{
	return mBounds;
}
// mPosition
template <typename T>
void SpriteBrick<T>::SetPosition(D3DXVECTOR3 position)
{
	mPosition = position;
	mBounds.BuildBoxFromTopLeft(mPosition);
}

template <typename T>
void SpriteBrick<T>::Draw()
{
	HRESULT drawSprite = mD3DSprite->Draw(
		mD3DTex,
		0,
		0,
		&mPosition,
		D3DCOLOR_XRGB(255, 255, 255));
	switch(drawSprite)
	{
	case D3DERR_INVALIDCALL:
		ErrorBox(L"Can't draw sprite: Invalid parameter(s).");
		HR(drawSprite);
		break;
	case D3DXERR_INVALIDDATA:
		ErrorBox(L"Can't draw sprite: Data corrupt/incorrect.");
		HR(drawSprite);
		break;
	}
}