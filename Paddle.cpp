#include "BoundingBox.hpp"
#include "DXUtil.hpp"
#include "Paddle.hpp"
#include <string>

#include <cstdio>

using namespace std;

// Constructors and Destructor
Paddle::Paddle(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring fileName, unsigned midSegments, float windowHeight, float windowWidth)
: Sprite(d3dDevice, d3dSprite, fileName),
  mWidth(0.0f),
  mHeight(23.0f),
  mBounds(mPosition.x, mPosition.y, mWidth, mHeight),
  mSpeed(600.0f),
  mDefaultSegments(midSegments),
  mNumSegments(midSegments),
  mLeftEndWidth(0.0f),
  mMidSectionWidth(0.0f),
  mRightEndWidth(0.0f),
  mBallsLocked(false)
{
	ZeroMemory(&mLeftEnd, sizeof(RECT));
	ZeroMemory(&mMidSection, sizeof(RECT));
	ZeroMemory(&mRightEnd, sizeof(RECT));

	mLeftEnd.right = 3;
	mLeftEnd.bottom = 24;
	mMidSection.left = mLeftEnd.right;
	mMidSection.right = mMidSection.left + 1;
	mMidSection.bottom = mLeftEnd.bottom;
	mRightEnd.left = mMidSection.right;
	mRightEnd.right = mRightEnd.left + 3;
	mRightEnd.bottom = mMidSection.bottom;

	mLeftEndWidth = static_cast<float>(mLeftEnd.right - mLeftEnd.left);
	mMidSectionWidth = static_cast<float>(mMidSection.right - mMidSection.left);
	mRightEndWidth = static_cast<float>(mRightEnd.right - mRightEnd.left);

	SetNumSegments(mNumSegments);

	float distFromBottom = 12.0f;
	mPosition = D3DXVECTOR3((0.5f)*windowWidth-mWidth, windowHeight-mHeight-distFromBottom, 0.0f);
	mBounds = BoundingBox(mPosition.x, mPosition.y, mWidth, mHeight);
}
Paddle::~Paddle()
{
}

// Accessors and Mutators
// mBounds
BoundingBox Paddle::GetBounds()
{
	return mBounds;
}
// mSpeed
void Paddle::SetSpeed(float speed)
{
	0 < speed ? mSpeed = speed : 0;
}
float Paddle::GetSpeed()
{
	return mSpeed;
}
// mNumSegmets
void Paddle::SetNumSegments(unsigned numSegments)
{
	mNumSegments = numSegments;

	// Adding segments invalidates the computed width of the paddle.
	mWidth = mLeftEndWidth + mRightEndWidth + (mNumSegments * mMidSectionWidth);

	// Changing the width invalidates the width of the bounding box.
	mBounds.SetWidth(mWidth);
}
int Paddle::GetNumSegments()
{
	return mNumSegments;
}
// mLockedBalls
void Paddle::LockBallToPaddle(Ball* ball)
{
	ball->Lock();
	mLockedBalls.push_back(ball);
	mBallsLocked = true;
}
void Paddle::ReleaseLockedBalls()
{
	if(!mBallsLocked)
		return;

	mBallsLocked = false;

	D3DXVECTOR3 ballOrientation(0.0f, 0.0f, 0.0f);

	for(unsigned i = 0; i < mLockedBalls.size(); i++)
	{
		ballOrientation = mLockedBalls[i]->GetBounds().GetCenter() - mBounds.GetCenter();
		D3DXVec3Normalize(&ballOrientation, &ballOrientation);
		mLockedBalls[i]->SetOrientation(ballOrientation);
		mLockedBalls[i]->Unlock();

		// Ensure that the ball's orientation isn't recalculated again until
		// it clears the paddle for the first time.
		mLockedBalls[i]->Collide();
	}
	mLockedBalls.clear();
}
// mPosition
void Paddle::SetPosition(D3DXVECTOR3 position)
{
	if(mBallsLocked)
	{
		static D3DXVECTOR3 displacement(0.0f, 0.0f, 0.0f);
		displacement = position - mPosition;

		for(unsigned i = 0; i < mLockedBalls.size(); i++)
		{
			static D3DXVECTOR3 currBallPos(0.0f, 0.0f, 0.0f);
			currBallPos = mLockedBalls[i]->GetPosition();
			currBallPos += displacement;
			mLockedBalls[i]->SetPosition(currBallPos);
		}
	}

	mPosition = position;
	mBounds.BuildBoxFromTopLeft(mPosition);
	return;
}

void PaddleMoveLeft(Paddle* paddle, float dt)
{
	D3DXVECTOR3 paddlePos = paddle->GetPosition();
	paddlePos.x -= paddle->GetSpeed() * dt;
	paddle->SetPosition(paddlePos);
}
void PaddleMoveRight(Paddle* paddle, float dt)
{
	D3DXVECTOR3 paddlePos = paddle->GetPosition();
	paddlePos.x += paddle->GetSpeed() * dt;
	paddle->SetPosition(paddlePos);
}
void PaddleWallCollision(Paddle* paddle, const RECT& walls)
{
	D3DXVECTOR3 paddlePos = paddle->GetPosition();

	bool paddleInLeftWall = paddlePos.x < walls.left;
	if(paddleInLeftWall)
	{
		paddlePos.x = static_cast<float>(walls.left);
		paddle->SetPosition(paddlePos);
		return;
	}

	float paddleWidth = paddle->GetBounds().GetWidth();

	bool paddleInRightWall = walls.right < paddlePos.x + paddleWidth;
	if(paddleInRightWall)
	{
		paddlePos.x = walls.right - paddleWidth;
		paddle->SetPosition(paddlePos);
		return;
	}
}

void Paddle::Reset()
{
	SetNumSegments(mDefaultSegments);
}

void Paddle::Draw()
{
	// The paddle needs to be drawn in three parts.
	HRESULT drawSprite = mD3DSprite->Draw(
		mD3DTex,
		&mLeftEnd,
		0,
		&mPosition,
		D3DCOLOR_XRGB(255, 255, 255));
	switch(drawSprite)
	{
	case D3DERR_INVALIDCALL:
		ErrorBox(L"Can't draw sprite: Invalid parameter(s).");
		HR(drawSprite);
		break;
	case D3DXERR_INVALIDDATA:
		ErrorBox(L"Can't draw sprite: Data corrupt/incorrect.");
		HR(drawSprite);
		break;
	}

	static D3DXVECTOR3 drawPos(0.0f, 0.0f, 0.0f);
	drawPos = mPosition;

	for(unsigned i = 0; i < mNumSegments; i++)
	{
		drawPos.x = mPosition.x + mLeftEndWidth + (static_cast<float>(i) * mMidSectionWidth);
		drawSprite = mD3DSprite->Draw(
			mD3DTex,
			&mMidSection,
			0,
			&drawPos,
			D3DCOLOR_XRGB(255, 255, 255));
		switch(drawSprite)
		{
		case D3DERR_INVALIDCALL:
			ErrorBox(L"Can't draw sprite: Invalid parameter(s).");
			HR(drawSprite);
			break;
		case D3DXERR_INVALIDDATA:
			ErrorBox(L"Can't draw sprite: Data corrupt/incorrect.");
			HR(drawSprite);
			break;
		}
	}

	drawPos.x += mMidSectionWidth;

	drawSprite = mD3DSprite->Draw(
		mD3DTex,
		&mRightEnd,
		0,
		&drawPos,
		D3DCOLOR_XRGB(255, 255, 255));
	switch(drawSprite)
	{
	case D3DERR_INVALIDCALL:
		ErrorBox(L"Can't draw sprite: Invalid parameter(s).");
		HR(drawSprite);
		break;
	case D3DXERR_INVALIDDATA:
		ErrorBox(L"Can't draw sprite: Data corrupt/incorrect.");
		HR(drawSprite);
		break;
	}
}