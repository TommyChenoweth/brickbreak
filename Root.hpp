#pragma once

class Root
{
private:
	float	mValue;
	bool	mReal;

public:
	// Constructors and Destructor
	Root(float value, bool real);
	Root();
	~Root();

	// Accessors and Mutators
	// mValue
	float GetValue();
	void SetValue(float value);
	// mImaginary
	bool IsReal();
	void SetImaginary();
	void SetReal();
};