#pragma once

#include <d3dx9core.h>
#include <iostream>

using namespace std;

class LineEquation
{
private:
	float mY;
	float mM;
	float mX;
	float mB;

public:
	// Constructors and Destructor
	LineEquation(float m, float b);
	LineEquation();
	~LineEquation();

	// Accessors and Mutators
	// mY
	float GetY() const;
	void SetY(float y);
	// mM
	float GetM() const;
	void SetM(float m);
	// mX
	float GetX() const;
	void SetX(float x);
	// mB
	float GetB() const;
	void SetB(float b);

	// Overloaded Operators
	friend ostream& operator<<(ostream& os, LineEquation& le);
};

float SolveForY(const LineEquation& line);
float SolveForX(const LineEquation& line);
float SolveForB(const LineEquation& line);
float DetermineSlope(D3DXVECTOR3 pointA, D3DXVECTOR3 pointB);