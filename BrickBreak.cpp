#include "Ball.hpp"
#include <boost/lexical_cast.hpp>
#include <locale>
#include "Brick.hpp"
#include "BrickBreak.hpp"
#include "DXUtil.hpp"
#include "GameTimer.hpp"
#include <irrKlang.h>
#include "Keyboard.hpp"
#include "LevelReader.hpp"
#include "Lives.hpp"
#include "MarbleBrick.hpp"
#include <memory>
#include "NormalBrick.hpp"
#include "Paddle.hpp"
#include "PowerMod.hpp"
#include <string>
#include "TitaniumBrick.hpp"
#include <windows.h>

using namespace std;
using namespace irrklang;
using namespace boost;

string WStringToString(wstring wstr);

// These are forward declarations for functions used in the message handler.
void HideCursor();
void ShowCursor();

BrickBreak::BrickBreak(HINSTANCE instance, unsigned width, unsigned height, wstring title)
: D3DApp(instance, width, height, title),
  mMainBall(0),
  mPaddle(0),
  mCenterPt(0),
  mHUD(0),
  mBackGround(0),
  mScore(0),
  mLives(0),
  mInput(0),
  mIKEngine(0),
  mMusic(0)
{
	mMainBall = new Ball(mD3DDevice, mD3DSprite, L"Graphics/ball.dds");
	mPaddle = new Paddle(mD3DDevice, mD3DSprite,L"Graphics/paddle.dds", 100, static_cast<float>(mWindowHeight), static_cast<float>(mWindowWidth));
	#if defined(DEBUG) | defined(_DEBUG)
		mCenterPt = new Sprite(mD3DDevice, mD3DSprite, L"Graphics/cp.dds");
	#endif
	mHUD = new Sprite(mD3DDevice, mD3DSprite, L"Graphics/hudbar.dds");
	mBackGround = new Background(mD3DDevice, mD3DSprite, L"Graphics/bgtile.dds", 100.0f, 100.0f, static_cast<float>(mWindowWidth), static_cast<float>(mWindowHeight));
	mScore = new Score(mD3DDevice, mD3DSprite, L"Graphics/digits.dds", 256.0f, 128.0f, 32.0f, 64.0f);
	mLives = new Lives(mD3DDevice, mD3DSprite, 3);
	mInput = new Keyboard();

	InitializeIrrKlang();

	mLevelData = BuildLevelFromFile(L"level.bbl");

	mMusic = mIKEngine->play2D(WStringToString(mLevelData->mMusicFilename).c_str(), true, false, true, ESM_AUTO_DETECT, true);
	mMusic->setPlayPosition(mLevelData->mPlayStart);

	SetWindowText(mWindow, mLevelData->mLevelName.c_str());

	NormalBrick::LoadTexture(mD3DDevice, mD3DSprite, L"Graphics/brick_normal.dds");
	NormalBrick::LoadSound(mIKEngine, "SFX/Brick_HitNormal.mp3");
	MarbleBrick::LoadTexture(mD3DDevice, mD3DSprite, L"Graphics/brick_marble.dds");
	MarbleBrick::LoadSound(mIKEngine, "SFX/Brick_HitMarble.mp3");
	TitaniumBrick::LoadTexture(mD3DDevice, mD3DSprite, L"Graphics/brick_titanium.dds");
	TitaniumBrick::LoadSound(mIKEngine, "SFX/Brick_HitTitanium.mp3");
	PowerMod::LoadTexture(mD3DDevice, mD3DSprite, L"Graphics/powermods.dds");
	PowerMod::LoadSFX(mIKEngine, "SFX/PowerMod.mp3");

	// Lock the ball to the paddle.
	D3DXVECTOR3 ballPos = mPaddle->GetPosition();
	ballPos.y -= mMainBall->GetBounds().GetRadius() * 2;
	ballPos.x += (mPaddle->GetBounds().GetWidth() * 0.5f) - mMainBall->GetBounds().GetRadius();
	mMainBall->SetPosition(ballPos);
	mPaddle->LockBallToPaddle(mMainBall);

	PassVarsToPMActions();
}
BrickBreak::~BrickBreak()
{
	DeletePtr(mMainBall);
	DeletePtr(mPaddle);
	DeletePtr(mCenterPt);
	DeletePtr(mHUD);
	DeletePtr(mBackGround);
	DeletePtr(mScore);
	DeletePtr(mLives);
	DeletePtr(mInput);
	PowerMod::ReleaseTexture();
	NormalBrick::ReleaseTexture();
	MarbleBrick::ReleaseTexture();
	TitaniumBrick::ReleaseTexture();
	mMusic->drop();
	if(mIKEngine)
		mIKEngine->drop();
}
void BrickBreak::InitializeIrrKlang()
{
	mIKEngine = createIrrKlangDevice();
	if(!mIKEngine)
		ErrorBox(L"Failed to initialize irrKlang engine!");
}
int BrickBreak::Run()
{
	MSG msg;
	msg.message = WM_NULL;
	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			if(mTimer->IsPaused())
			{
				Sleep(20);
				continue;
			}
			if(!IsDeviceLost())
			{
				// Update and draw the scene.
				mTimer->UpdateTimer();
				mInput->PollMouse(mWindow);
				mInput->PollKeys();
				UpdateScene(mTimer->GetDeltaT());
				DrawScene();
			}
		}
	}
	return (int)msg.wParam;
}

void BrickBreak::DrawScene()
{
	HR(mD3DDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x0000ff, 1.0f, 0));
	HR(mD3DDevice->BeginScene());

	mD3DSprite->Begin(D3DXSPRITE_ALPHABLEND);

	mBackGround->Draw();
	for(unsigned i = 0; i < mLevelData->mBricks.size(); i++)
	{
		if(!mLevelData->mBricks[i]->IsDestroyed())
			mLevelData->mBricks[i]->Draw();
	}
	mMainBall->Draw();
	for(unsigned i = 0; i < mPowerMods.size(); i++)
	{
		mPowerMods[i].Draw();
	}
	mPaddle->Draw();
	#if defined(DEBUG) | defined(_DEBUG)
		mCenterPt->SetPosition(mMainBall->GetBounds().GetCenter());
		mCenterPt->Draw();
		mCenterPt->SetPosition(mPaddle->GetBounds().GetCenter());
		mCenterPt->Draw();
	#endif
	mHUD->Draw();
	mScore->Draw();
	mLives->Draw();

	mTimer->DrawTimerText();

	mD3DSprite->End();
	
	HR(mD3DDevice->EndScene());
	HR(mD3DDevice->Present(0, 0, 0, 0));
}



LRESULT BrickBreak::UserInput(UINT msg, WPARAM wParam, LPARAM lParam)
{
	static D3DXVECTOR3 mousePos(0.0f, 0.0f, 0.0f);

	switch(msg)
	{
	case WM_RBUTTONDOWN:
		if( wParam & MK_RBUTTON )
		{
		}
		return 0;

	case WM_RBUTTONUP:
		return 0;

	case WM_MOUSEMOVE:
		if(mTimer->IsPaused())
		{
			ShowCursor();
		}
		else
		{
			HideCursor();
		}
		mInput->SetMouseActive();
		return 0;
	case WM_MOUSELEAVE:
		ShowCursor(true);
		return 0;
	case WM_KEYDOWN:
		mInput->SetMouseInactive();
		if(wParam == VK_ESCAPE) 
		{
			if(mTimer->IsPaused())
			{
				mTimer->Resume();
				HideCursor();

				// Resume music if it's paused.
				if(mMusic && mMusic->getIsPaused())
					mMusic->setIsPaused(false);
			}
			else
			{
				mTimer->Pause();
				ShowCursor();

				// Pause music if it's playing.
				if(mMusic && !mMusic->getIsPaused())
					mMusic->setIsPaused(true);
			}
		}
		return 0;
	}

	return msgProc(msg, wParam, lParam);
}

void BrickBreak::LifeLost()
{
	if(mLives->GetNumLives() == 0)
	{
		// The player has no more lives left, so the game should be reset.
		for(unsigned i = 0; i < mLevelData->mBricks.size(); i++)
		{
			mLevelData->mBricks[i]->Reset();
		}

		mLives->ResetLives();
		mScore->SetScore(0);
		mPowerMods.clear();
	}
	else
	{
		mLives->RemoveLife();
		mScore->AddToScore(-5000);
	}

	// Normalize the music speed. Powerups could have caused the playspeed to
	// increase or decrease.
	mMusic->setPlaybackSpeed(1.0f);

	// Restore the ball to default speed. Powerups could have caused the ball
	// speed to increase or decrease.
	mMainBall->SetSpeed(800);

	mPaddle->Reset();

	// Lock the ball to the paddle.
	D3DXVECTOR3 ballPos = mPaddle->GetPosition();
	ballPos.y -= mMainBall->GetBounds().GetRadius() * 2;
	ballPos.x += (mPaddle->GetBounds().GetWidth() * 0.5f) - mMainBall->GetBounds().GetRadius();
	mMainBall->SetPosition(ballPos);
	mPaddle->LockBallToPaddle(mMainBall);
}

int gDisplayCount(0);

void HideCursor()
{
	// The cursor will be hidden if the display count is less than zero.
	while(0 <= gDisplayCount)
		gDisplayCount = ShowCursor(false);
}
void ShowCursor()
{
	// The cursor will only show if the display count is greater than or equal to zero.
	while(gDisplayCount < 0)
		gDisplayCount = ShowCursor(true);
}

string WStringToString(wstring wstr)
{
	string str("");
	std::locale loc;
	for(unsigned i = 0; i < wstr.size(); i++)
	{
		str += std::use_facet<std::ctype<wchar_t>>(loc).narrow(wstr[i]);
	}
	return str;
}