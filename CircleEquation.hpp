#pragma once

#include <iostream>
#include <utility>
#include <d3dx9core.h>
#include "Solution.hpp"

using namespace std;

class CircleEquation
{
private:
	float		mR;
	float		mX;
	float		mH;
	float		mY;
	float		mK;
	D3DXVECTOR3 mCenter;

public:
	// Constuctors and Destructor
	CircleEquation(float r, float h, float k);
	CircleEquation();
	~CircleEquation();

	// Accessors and Mutators
	// mR
	float GetR() const;
	void SetR(float r);
	// mX
	float GetX() const;
	void SetX(float x);
	// mH
	float GetH() const;
	void SetH(float h);
	// mY
	float GetY() const;
	void SetY(float y);
	// mK
	float GetK() const;
	void SetK(float k);
	// mCenter
	D3DXVECTOR3 GetCenter() const;

	// Overloaded Operators
	friend ostream& operator<<(ostream& os, CircleEquation& circle);
};

pair<Solution, Solution> SolveForY(const CircleEquation& circle);
pair<Solution, Solution> SolveForX(const CircleEquation& circle);
bool IsPointInCircle(const D3DXVECTOR3& point, const CircleEquation& circle);