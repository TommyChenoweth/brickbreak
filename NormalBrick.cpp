#include "NormalBrick.hpp"
#include "SpriteBrick.hpp"

// Constructors and Destructors
NormalBrick::NormalBrick(unsigned width, unsigned height)
: SpriteBrick<NormalBrick>(width, height)
{
}
NormalBrick::~NormalBrick()
{
}

int NormalBrick::Struck()
{
	PlaySFX();
	mDestroyed = true;
	return mPoints;
}