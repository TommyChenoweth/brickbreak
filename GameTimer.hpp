#pragma once

#include <d3d9.h>
#include <d3dx9core.h>
#include <windows.h>
#include <string>

using namespace std;

class GameTimer
{
private:
	ID3DXFont*		mD3DFont;
	ID3DXSprite*	mD3DSprite;

	D3DCOLOR		mTextColor;

	int				mPauseCount;
	bool			mPaused;
	bool			mResumed;
	__int64			mCntsPrevFrame;
	__int64			mCntsCurrFrame;
	float			mSecondsPerCnt;
	float			dt;

	float			mFPS;
	float			mMSPerFrame;

	wstring			mTextDisplay;

	__int64			GetCounts();

public:
	// Constructor
	GameTimer(ID3DXFont* d3dFont, ID3DXSprite* d3dSprite);

	//Accessors and Mutators
	float GetDeltaT();
	void SetTextColor(D3DCOLOR textColor);
	void Pause();
	void Resume();
	bool IsPaused();

	void UpdateTimer();
	void DrawTimerText();
};