#include "Brick.hpp"
#include <d3dx9core.h>

// Constructors and Destructor
Brick::Brick()
: mDestroyed(false),
  mPoints(999)
{
}
Brick::~Brick()
{
}

// Accessors and Mutators.
// mDestroyed
bool Brick::IsDestroyed()
{
	return mDestroyed;
}

int Brick::Struck()
{
	mDestroyed = true;
	return mPoints;
}
void Brick::Reset()
{
	mDestroyed = false;
}