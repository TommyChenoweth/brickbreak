#include <windows.h>
#include <boost/dynamic_bitset.hpp>
#include "Keyboard.hpp"

// Constructors and Destructor
Keyboard::Keyboard()
: mMousePosition(0.0f, 0.0f, 0.0f),
  mMouseActive(true)
{
	mKeyStatus.resize(4, false);
}
Keyboard::~Keyboard()
{
}

// Accessors and Mutators
// mKeyStatus
void Keyboard::ResetKeys()
{
	for(dynamic_bitset<>::size_type i = 0; i < mKeyStatus.size(); i++)
	{
		mKeyStatus[i] = false;
	}
}
void Keyboard::PollKeys()
{
	mKeyStatus[KEY_LEFTARROW]		= (GetAsyncKeyState(VK_LEFT)&0x8000);
	mKeyStatus[KEY_RIGHTARROW]		= (GetAsyncKeyState(VK_RIGHT)&0x8000);
	mKeyStatus[KEY_ESCAPE]			= (GetAsyncKeyState(VK_ESCAPE)&0x8000);
	mKeyStatus[MOUSE_LEFTBUTTON]	= (GetAsyncKeyState(VK_LBUTTON)&0x8000);
}
void Keyboard::KeyDown(unsigned key)
{
	mKeyStatus[key] = true;
}
void Keyboard::KeyUp(unsigned key)
{
	mKeyStatus[key] = false;
}
bool Keyboard::IsKeyDown(unsigned key)
{
	return mKeyStatus[key];
}
// mMousePosition
void Keyboard::PollMouse(HWND hwnd)
{
	static POINT pt;
	GetCursorPos(&pt);
	ScreenToClient(hwnd, &pt);
	mMousePosition.x = static_cast<float>(pt.x);
	mMousePosition.y = static_cast<float>(pt.y);
}
D3DXVECTOR3 Keyboard::GetMousePosition()
{
	return mMousePosition;
}
void Keyboard::SetMousePosition(D3DXVECTOR3 position)
{
	mMousePosition = position;
}
// mMouseActive
bool Keyboard::IsMouseActive()
{
	return mMouseActive;
}
void Keyboard::SetMouseActive()
{
	mMouseActive = true;
}
void Keyboard::SetMouseInactive()
{
	mMouseActive = false;
}