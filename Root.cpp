#include "Root.hpp"

// Constructors and Destructor
Root::Root(float value, bool real)
: mValue(value),
  mReal(real)
{
}
Root::Root()
: mValue(0.0f),
  mReal(true)
{
}
Root::~Root()
{
}

// Accessors and Mutators
// mValue
float Root::GetValue()
{
	return mValue;
}
void Root::SetValue(float value)
{
	mValue = value;
}
// mImaginary
bool Root::IsReal()
{
	return mReal;
}
void Root::SetImaginary()
{
	mReal = false;
}
void Root::SetReal()
{
	mReal = true;
}