#pragma once

#include <d3d9.h>
#include <D3dx9core.h>
#include <string>
#include "Sprite.hpp"
#include "BoundingSphere.hpp"

using namespace std;

class Ball : public Sprite
{
private:
	D3DXVECTOR3		mOrientation;
	D3DXVECTOR3		mCenter;
	BoundingSphere*	mBounds;
	unsigned		mSpeed;
	bool			mLocked;
	bool			mColliding;

public:
	// Constructors and Destructor
	Ball(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring fileName);
	~Ball();

	// Accessors and Mutators
	// mPosition
	void SetPosition(D3DXVECTOR3 position);
	// mOrientation
	D3DXVECTOR3 GetOrientation();
	void SetOrientation(D3DXVECTOR3 orientation);
	// mBoundingSphere
	BoundingSphere GetBounds();
	// mSpeed
	unsigned GetSpeed();
	void SetSpeed(unsigned speed);
	// mLocked
	void Lock();
	void Unlock();
	bool IsLocked();
	// mColliding
	bool AlreadyCollided();
	void Collide();
	void DoneColliding();

	void Draw();
};

//void BallMove(Ball* ball, float dt);
void BallWallCollision(Ball* ball, const RECT& walls);