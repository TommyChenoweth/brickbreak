#include <d3d9.h>
#include <tchar.h>
#include "D3DApp.hpp"
#include "DXUtil.hpp"
#include "GameTimer.hpp"

// Forward Declarations
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

D3DApp::D3DApp(HINSTANCE instance, unsigned width, unsigned height, wstring title)
: mD3DObject(0),
  mD3DDevice(0),
  mD3DSprite(0),
  mD3DFont(0),
  mAppPaused(false),
  mWindowWidth(width),
  mWindowHeight(height),
  mTitle(title),
  mTimer(0)
{
	mAdapter = D3DADAPTER_DEFAULT;
	mRequestedVP = D3DCREATE_HARDWARE_VERTEXPROCESSING;
	//mRequestedVP = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	mDevBehaviorFlags = 0;
	mDevType = D3DDEVTYPE_HAL;
	mAdptrFormat = D3DFMT_X8R8G8B8;
	mBBFormat = D3DFMT_X8R8G8B8;

	ZeroMemory(&mD3DPresentParams, sizeof(mD3DPresentParams));
	mInstance = instance;
	mWindow = 0;

	// The order here is important.
	InitMainWindow();
	InitD3D();
	CreateSprite();
	CreateFont();

	mTimer = new GameTimer(mD3DFont, mD3DSprite);
}

D3DApp::~D3DApp()
{
	ReleaseCOM(mD3DSprite);
	ReleaseCOM(mD3DDevice);
	ReleaseCOM(mD3DObject);
	ReleaseCOM(mD3DFont);
	DeletePtr(mTimer);
}	

void D3DApp::InitMainWindow()
{
	WNDCLASS wc;
	ZeroMemory(&wc, sizeof(wc));
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = MainWndProc; 
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = mInstance;
	wc.hIcon         = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor       = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	wc.lpszMenuName  = 0;
	wc.lpszClassName = L"D3DWndClassName";

	if( !RegisterClass(&wc) )
	{
		ErrorBox(L"RegisterClass FAILED");
		PostQuitMessage(0);
	}

	// Compute window rectangle dimensions based on requested client area dimensions.
	RECT R = { 0, 0, mWindowWidth, mWindowHeight };
	AdjustWindowRect(&R, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, false);
	int width  = R.right - R.left;
	int height = R.bottom - R.top;

	mWindow = CreateWindow(L"D3DWndClassName", mTitle.c_str(), 
		WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, CW_USEDEFAULT, CW_USEDEFAULT, width, height, 0, 0, mInstance, this); 
	if( !mWindow )
	{
		ErrorBox(L"CreateWindow FAILED");
		PostQuitMessage(0);
	}

	ShowWindow(mWindow, SW_SHOW);
	UpdateWindow(mWindow);
}

void D3DApp::InitD3D()
{
	// These functions return HRESULTS because it may be possible
	// to recover from the errors; However, I'm not sure how to do it right
	// now. The return types are place holders for that.
	/* 1) Acquire an IDirect3D9 Interface. */
	if(FAILED(CreateD3DObject()))
		PostQuitMessage(0);

	/* 2) Verify HAL Support. */
	if(FAILED(VerifyHALAndFormatSupport()))
		PostQuitMessage(0);

	/* 3) Check for hardware vertex processing. */
	if(FAILED(VerifyHardwareVertexProcessing()))
		PostQuitMessage(0);

	/* 4) Fill out the present parameters. */
	SetPresentParams();

	/* 5) Create an IDirect3DDevice9 Interface. */
	if(FAILED(CreateDeviceInterface()))
		PostQuitMessage(0);

}

HRESULT D3DApp::CreateD3DObject()
{
	mD3DObject = Direct3DCreate9(D3D_SDK_VERSION);
	if(!mD3DObject)	// If the object wasn't initialized...
	{
		ErrorBox(L"Couldn't create a D3D Object.");
		return E_FAIL;
	}
	return D3D_OK;
}

HRESULT D3DApp::VerifyHALAndFormatSupport()
{
	D3DDISPLAYMODE mode;
	mD3DObject->GetAdapterDisplayMode(mAdapter, &mode);

	// Verify windowed support of HAL. We'll use whatever
	// format that the adapter is currently using.
	HRESULT supportWindowed;
	supportWindowed = mD3DObject->CheckDeviceType(
		mAdapter,
		mDevType,
		mode.Format,
		mode.Format,
		true);
	switch(supportWindowed)
	{
	case D3DERR_INVALIDCALL:
		// A parameter is invalid, incorrect, or corrupt.
		HR(supportWindowed);
		return E_FAIL;
	case D3DERR_NOTAVAILABLE:
		// The requested format isn't supported by the adapter.
		HR(supportWindowed);
		return E_FAIL;
	}

	// Verify fullscreen support of HAL and format.
	HRESULT supportFullscreen;
	supportFullscreen = mD3DObject->CheckDeviceType(
		mAdapter,
		mDevType,
		mAdptrFormat,
		mBBFormat,
		false);
	switch(supportFullscreen)
	{
	case D3DERR_INVALIDCALL:
		// A parameter is invalid, incorrect, or corrupt.
		HR(supportFullscreen);
		return E_FAIL;
	case D3DERR_NOTAVAILABLE:
		// The requested format isn't supported by the adapter.
		HR(supportFullscreen);
		return E_FAIL;
	}

	return D3D_OK;
}

HRESULT D3DApp::VerifyHardwareVertexProcessing()
{
	D3DCAPS9 caps;
	HR(mD3DObject->GetDeviceCaps(mAdapter, mDevType, &caps));

	// If the device supports transform and light, we can use whichever
	// vertex processing we please.
	if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		mDevBehaviorFlags |= mRequestedVP;
	else
	{
		// Hardware vertex processing isn't available. The only vertex
		// processing we can use is software.
		ErrorBox(L"Hardware vertex processing isn't available. Using software instead.");
		mDevBehaviorFlags |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	}

	// Make a pure device if possible.
	bool HardwareVPAndPureDeviceSupported = false;
	if(caps.DevCaps & D3DDEVCAPS_PUREDEVICE && 
	   mDevBehaviorFlags & D3DCREATE_HARDWARE_VERTEXPROCESSING)
	{
		HardwareVPAndPureDeviceSupported = true;
	}
	if(HardwareVPAndPureDeviceSupported)
	{
		// Yes we can.
		mDevBehaviorFlags |= D3DCREATE_PUREDEVICE;
		return D3D_OK;
	}

	return D3D_OK;
}

void D3DApp::SetPresentParams()
{
	mD3DPresentParams.BackBufferWidth				= 0;
	mD3DPresentParams.BackBufferHeight				= 0;
	mD3DPresentParams.BackBufferFormat				= D3DFMT_UNKNOWN;
	mD3DPresentParams.BackBufferCount				= 1;
	mD3DPresentParams.MultiSampleType				= D3DMULTISAMPLE_NONE;
	mD3DPresentParams.MultiSampleQuality			= 0;
	mD3DPresentParams.SwapEffect					= D3DSWAPEFFECT_DISCARD;
	mD3DPresentParams.hDeviceWindow					= mWindow;
	mD3DPresentParams.Windowed						= true;
	mD3DPresentParams.EnableAutoDepthStencil		= true;
	mD3DPresentParams.AutoDepthStencilFormat		= D3DFMT_D24S8;
	mD3DPresentParams.Flags							= 0;
	mD3DPresentParams.FullScreen_RefreshRateInHz	= D3DPRESENT_RATE_DEFAULT;
	#if defined(DEBUG) | defined(_DEBUG)
	//mD3DPresentParams.PresentationInterval			= D3DPRESENT_INTERVAL_IMMEDIATE;
	mD3DPresentParams.PresentationInterval			= D3DPRESENT_INTERVAL_DEFAULT;
	#else
	//mD3DPresentParams.PresentationInterval			= D3DPRESENT_INTERVAL_IMMEDIATE;
	mD3DPresentParams.PresentationInterval			= D3DPRESENT_INTERVAL_DEFAULT;
	#endif
}

HRESULT D3DApp::CreateDeviceInterface()
{
	HRESULT deviceResult = mD3DObject->CreateDevice(
		mAdapter,
		mDevType,
		mWindow,
		mDevBehaviorFlags,
		&mD3DPresentParams,
		&mD3DDevice);

	switch(deviceResult)
	{
	case D3DERR_DEVICELOST:
		// Somehow the device is already lost. I don't know whether
		// or not it can be recovered at this point.
		ErrorBox(L"Can't create device: The device is lost.");
		return E_FAIL;
	case D3DERR_INVALIDCALL:
		// A parameter is incorrect, corrupt, or invalid.
		HR(deviceResult);
		return E_FAIL;
	case D3DERR_NOTAVAILABLE:
		// This device does not support the queried technique.
		ErrorBox(L"Can't create device: The adapter doesn't support a feature.");
		return E_FAIL;
	case D3DERR_OUTOFVIDEOMEMORY:
		// Somehow the adapter is already out of video memory.
		ErrorBox(L"Can't create device: Out of video memory.");
		return E_FAIL;
	}

	return D3D_OK;
}

void D3DApp::CreateSprite()
{
	HRESULT createSprite = D3DXCreateSprite(mD3DDevice, &mD3DSprite);
	switch(createSprite)
	{
	case D3DERR_INVALIDCALL:
		ErrorBox(L"Can't create sprite: Invalid parameters.");
		PostQuitMessage(0);
		break;
	case E_OUTOFMEMORY:
		ErrorBox(L"Can't create sprite: Out of memory.");
		PostQuitMessage(0);
		break;
	}
}

void D3DApp::CreateFont()
{
	D3DXFONT_DESC fontDesc;
	fontDesc.Height				= 18;
	fontDesc.Width				= 0;
	fontDesc.Weight				= 0;
	fontDesc.MipLevels			= 1;
	fontDesc.Italic				= false;
	fontDesc.CharSet			= DEFAULT_CHARSET;
	fontDesc.OutputPrecision	= OUT_DEFAULT_PRECIS;
	fontDesc.Quality			= DEFAULT_QUALITY;
	fontDesc.PitchAndFamily		= DEFAULT_PITCH | FF_DONTCARE;

	wstring fontName(L"Times New Roman");
	wcsncpy_s(fontDesc.FaceName, sizeof(fontDesc.FaceName)/sizeof(WORD), fontName.c_str(), 32);

	D3DXCreateFontIndirect(mD3DDevice, &fontDesc, &mD3DFont);
}

void D3DApp::OnLostDevice()
{
	if(mD3DSprite) mD3DSprite->OnLostDevice();
	if(mD3DFont) mD3DFont->OnLostDevice();
}

void D3DApp::OnResetDevice()
{
	if(mD3DSprite) mD3DSprite->OnResetDevice();
	if(mD3DFont) mD3DFont->OnResetDevice();
}

bool D3DApp::IsDeviceLost()
{
	HRESULT deviceStatus = mD3DDevice->TestCooperativeLevel();

	switch(deviceStatus)
	{
	case D3DERR_DEVICELOST:
		// The device is lost. Wait until we have the opportunity
		// to reset it.
		Sleep(20);
		return true;
	case D3DERR_DRIVERINTERNALERROR:
		// Internal driver error. Not much we can do about this. Quit.
		ErrorBox(L"Internal Driver Error!");
		PostQuitMessage(0);
		return true;
	case D3DERR_DEVICENOTRESET:
		// The device is lost, but we can reset and restore it.
		OnLostDevice();
		HR(mD3DDevice->Reset(&mD3DPresentParams));
		OnResetDevice();
		// We've got the device back.
		return false;
	}

	return false;
}

int D3DApp::Run()
{
	MSG msg;
	msg.message = WM_NULL;
	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			if(mAppPaused)
			{
				Sleep(20);
				continue;
			}
			// If the device isn't lost...
			if(!IsDeviceLost())
			{
				// Update and draw the scene.
				mTimer->UpdateTimer();
				UpdateScene(mTimer->GetDeltaT());
				DrawScene();
			}
		}
	}
	return (int)msg.wParam;
}

void D3DApp::UpdateScene(float dt)
{
}

void D3DApp::DrawScene()
{
	HR(mD3DDevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x0000ff, 1.0f, 0));
	HR(mD3DDevice->BeginScene());

	mD3DSprite->Begin(D3DXSPRITE_ALPHABLEND);

	mTimer->DrawTimerText();

	mD3DSprite->End();
	
	HR(mD3DDevice->EndScene());
	HR(mD3DDevice->Present(0, 0, 0, 0));
}

LRESULT D3DApp::msgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	// Is the application in a minimized or maximized state?
	static bool minOrMaxed = false;

	RECT clientRect = {0, 0, 0, 0};
	switch( msg )
	{
	case WM_MOVING:
		//mTimer->Pause();
		return 0;
	case WM_MOVE:
		//mTimer->Resume();
		return 0;
	// WM_ACTIVE is sent when the window is activated or deactivated.
	// We pause the game when the main window is deactivated and 
	// unpause it when it becomes active.
	case WM_ACTIVATE:
		if( LOWORD(wParam) == WA_INACTIVE)
		{
			mTimer->Pause();
		}
		else
		{
			mTimer->Resume();
		}
		return 0;

	// WM_SIZE is sent when the user resizes the window.  
	case WM_SIZE:
		if( mD3DDevice )
		{
			mD3DPresentParams.BackBufferWidth  = LOWORD(lParam);
			mD3DPresentParams.BackBufferHeight = HIWORD(lParam);

			if( wParam == SIZE_MINIMIZED )
			{
				mTimer->Pause();
				minOrMaxed = true;
			}
			else if( wParam == SIZE_MAXIMIZED )
			{
				mTimer->Resume();
				minOrMaxed = true;
				OnLostDevice();
				HR(mD3DDevice->Reset(&mD3DPresentParams));
				OnResetDevice();
			}
			// Restored is any resize that is not a minimize or maximize.
			// For example, restoring the window to its default size
			// after a minimize or maximize, or from dragging the resize
			// bars.
			else if( wParam == SIZE_RESTORED )
			{
				mTimer->Resume();

				// Are we restoring from a mimimized or maximized state, 
				// and are in windowed mode?  Do not execute this code if 
				// we are restoring to full screen mode.
				if( minOrMaxed && mD3DPresentParams.Windowed )
				{
					OnLostDevice();
					HR(mD3DDevice->Reset(&mD3DPresentParams));
					OnResetDevice();
				}
				else
				{
					// No, which implies the user is resizing by dragging
					// the resize bars.  However, we do not reset the device
					// here because as the user continuously drags the resize
					// bars, a stream of WM_SIZE messages is sent to the window,
					// and it would be pointless (and slow) to reset for each
					// WM_SIZE message received from dragging the resize bars.
					// So instead, we reset after the user is done resizing the
					// window and releases the resize bars, which sends a
					// WM_EXITSIZEMOVE message.
				}
				minOrMaxed = false;
			}
		}
		return 0;


	// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
	// Here we reset everything based on the new window dimensions.
	case WM_EXITSIZEMOVE:
		GetClientRect(mWindow, &clientRect);
		mD3DPresentParams.BackBufferWidth  = clientRect.right;
		mD3DPresentParams.BackBufferHeight = clientRect.bottom;
		OnLostDevice();
		HR(mD3DDevice->Reset(&mD3DPresentParams));
		OnResetDevice();

		return 0;

	// WM_CLOSE is sent when the user presses the 'X' button in the
	// caption bar menu.
	case WM_CLOSE:
		DestroyWindow(mWindow);
		return 0;

	// WM_DESTROY is sent when the window is being destroyed.
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_KEYDOWN:
		if( wParam == VK_ESCAPE )
			EnableFullScreenMode(false);
		else if( wParam == 'F' )
			EnableFullScreenMode(true);
		return 0;
	}
	return DefWindowProc(mWindow, msg, wParam, lParam);
}

LRESULT D3DApp::UserInput(UINT msg, WPARAM wParam, LPARAM lParam)
{
	switch(msg)
	{
	case WM_RBUTTONDOWN:
		if( wParam & MK_RBUTTON )
		{
		}
		return 0;

	case WM_RBUTTONUP:
		return 0;

	case WM_MOUSEMOVE:
		return 0;
	}

	return msgProc(msg, wParam, lParam);
}

void D3DApp::EnableFullScreenMode(bool enable)
{
	// Switch to fullscreen mode.
	if( enable )
	{
		// Are we already in fullscreen mode?
		if( !mD3DPresentParams.Windowed ) 
			return;

		int width  = GetSystemMetrics(SM_CXSCREEN);
		int height = GetSystemMetrics(SM_CYSCREEN);

		mD3DPresentParams.BackBufferFormat = mBBFormat;
		mD3DPresentParams.BackBufferWidth  = width;
		mD3DPresentParams.BackBufferHeight = height;
		mD3DPresentParams.Windowed         = false;

		// Change the window style to a more fullscreen friendly style.
		SetWindowLongPtr(mWindow, GWL_STYLE, WS_POPUP);

		// If we call SetWindowLongPtr, MSDN states that we need to call
		// SetWindowPos for the change to take effect.  In addition, we 
		// need to call this function anyway to update the window dimensions.
		SetWindowPos(mWindow, HWND_TOP, 0, 0, width, height, SWP_NOZORDER | SWP_SHOWWINDOW);	
	}
	// Switch to windowed mode.
	else
	{
		// Are we already in windowed mode?
		if( mD3DPresentParams.Windowed ) 
			return;

		RECT R = {0, 0, mWindowWidth, mWindowHeight};
		AdjustWindowRect(&R, WS_OVERLAPPEDWINDOW, false);
		mD3DPresentParams.BackBufferFormat = D3DFMT_UNKNOWN;
		mD3DPresentParams.BackBufferWidth  = mWindowWidth;
		mD3DPresentParams.BackBufferHeight = mWindowHeight;
		mD3DPresentParams.Windowed         = true;
	
		// Change the window style to a more windowed friendly style.
		SetWindowLongPtr(mWindow, GWL_STYLE, WS_OVERLAPPEDWINDOW);

		// If we call SetWindowLongPtr, MSDN states that we need to call
		// SetWindowPos for the change to take effect.  In addition, we 
		// need to call this function anyway to update the window dimensions.
		SetWindowPos(mWindow, HWND_TOP, 100, 100, R.right, R.bottom, SWP_NOZORDER | SWP_SHOWWINDOW);
	}

	// Reset the device with the changes.
	OnLostDevice();
	HR(mD3DDevice->Reset(&mD3DPresentParams));
	OnResetDevice();
}

// This isn't part of the class:
LRESULT CALLBACK
MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static D3DApp* app = 0;

	switch( msg )
	{
		case WM_CREATE:
		{
			// Get the 'this' pointer we passed to CreateWindow via the lpParam parameter.
			CREATESTRUCT* cs = (CREATESTRUCT*)lParam;
			app = (D3DApp*)cs->lpCreateParams;
			return 0;
		}
	}

	// Don't start processing messages until after WM_CREATE.
	if( app )
		return app->UserInput(msg, wParam, lParam);
	else
		return DefWindowProc(hwnd, msg, wParam, lParam);
}