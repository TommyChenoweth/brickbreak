#include "DXUtil.hpp"
#include <irrKlang.h>
#include "PowerMod.hpp"
#include "SharedSprite.hpp"
#include <string>

using namespace std;
using namespace irrklang;

// Static Member Variables
const float PowerMod::mWidth = 45.0f;
const float PowerMod::mHeight = 45.0f;

ISoundSource* PowerMod::mActivateSound = 0;
ISoundEngine* PowerMod::mIKEngine = 0;

// Constructors and Destructors
PowerMod::PowerMod(D3DXVECTOR3 position)
: SharedSprite<PowerMod>(),
  mBounds(0.0f, 0.0f, mWidth, mHeight),
  Action(0),
  mFrame(0)
{
	SetPosition(position);
	mFrames[0].left = 0;
	mFrames[0].top = 0;
	mFrames[0].right = 45;
	mFrames[0].bottom = 45;

	mFrames[1].left = 45;
	mFrames[1].top = 0;
	mFrames[1].right = 90;
	mFrames[1].bottom = 45;

	mFrames[2].left = 0;
	mFrames[2].top = 45;
	mFrames[2].right = 45;
	mFrames[2].bottom = 90;

	mFrames[3].left = 45;
	mFrames[3].top = 45;
	mFrames[3].right = 90;
	mFrames[3].bottom = 90;
}
PowerMod::PowerMod()
: SharedSprite<PowerMod>(),
  mBounds(0.0f, 0.0f, mWidth, mHeight),
  Action(0),
  mFrame(1)
{
	mFrames[0].left = 0;
	mFrames[0].top = 0;
	mFrames[0].right = 45;
	mFrames[0].bottom = 45;

	mFrames[1].left = 45;
	mFrames[1].top = 0;
	mFrames[1].right = 90;
	mFrames[1].bottom = 45;

	mFrames[2].left = 0;
	mFrames[2].top = 45;
	mFrames[2].right = 45;
	mFrames[2].bottom = 90;

	mFrames[3].left = 45;
	mFrames[3].top = 45;
	mFrames[3].right = 90;
	mFrames[3].bottom = 90;
}
PowerMod::~PowerMod()
{
}

// Accessors and Mutators
// mPosition
void PowerMod::SetPosition(D3DXVECTOR3 position)
{
	mPosition = position;
	mBounds.BuildBoxFromTopLeft(mPosition);
}
// mBounds
BoundingBox PowerMod::GetBounds()
{
	return mBounds;
}
// mFrame
void PowerMod::SetFrame(unsigned frame)
{
	mFrame = frame;
}

void PowerMod::Draw()
{
	HRESULT drawSprite = mD3DSprite->Draw(
		mD3DTex,
		&mFrames[mFrame],
		0,
		&mPosition,
		D3DCOLOR_XRGB(255, 255, 255));
	switch(drawSprite)
	{
	case D3DERR_INVALIDCALL:
		ErrorBox(L"Can't draw sprite: Invalid parameter(s).");
		HR(drawSprite);
		break;
	case D3DXERR_INVALIDDATA:
		ErrorBox(L"Can't draw sprite: Data corrupt/incorrect.");
		HR(drawSprite);
		break;
	}
}

void PowerMod::LoadSFX(ISoundEngine* iKEngine, string fileName)
{
	mActivateSound = iKEngine->addSoundSourceFromFile(fileName.c_str(), ESM_AUTO_DETECT, false);
	mIKEngine = iKEngine;
}
void PowerMod::PlaySFX()
{
	mIKEngine->play2D(mActivateSound, false, false, false, false);
}
void PowerMod::DropSFX()
{
	if(mActivateSound)
	{
		mActivateSound->drop();
		mActivateSound = 0;
	}
}

// Non-Member Functions
bool PowerModWallCollision(PowerMod& mod, const RECT& walls)
{
	static D3DXVECTOR3 position(0.0f, 0.0f, 0.0f);
	position = mod.GetPosition();
	
	static bool powerModOffScreen = (static_cast<float>(walls.bottom) <= position.y);
	if(powerModOffScreen)
	{
		return true;
	}
	return false;
}