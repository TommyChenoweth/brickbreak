#pragma once

#include "Background.hpp"
#include "Ball.hpp"
#include "D3DApp.hpp"
#include <d3dx9core.h>
#include <irrKlang.h>
#include "Keyboard.hpp"
#include "LevelReader.hpp"
#include "Lives.hpp"
#include <memory>
#include "Paddle.hpp"
#include "PowerMod.hpp"
#include "Score.hpp"
#include "Sprite.hpp"
#include <string>
#include <vector>
#include <windows.h>

using namespace std;
using namespace irrklang;

class BrickBreak : public D3DApp
{
private:
	Ball*				mMainBall;
	Paddle*				mPaddle;
	Sprite*				mCenterPt;
	Sprite*				mHUD;
	Background*			mBackGround;
	Keyboard*			mInput;
	Score*				mScore;
	Lives*				mLives;
	auto_ptr<LevelData>	mLevelData;
	ISoundEngine*		mIKEngine;
	ISound*				mMusic;
	vector<PowerMod>	mPowerMods;

	void InitializeIrrKlang();

public:
	BrickBreak(HINSTANCE instance, unsigned width, unsigned height, wstring title);
	~BrickBreak();

	int Run();
	void UpdateScene(float dt);
	void DrawScene();
	virtual LRESULT UserInput(UINT msg, WPARAM wParam, LPARAM lParam);
	void LifeLost();

	void PassVarsToPMActions();
};