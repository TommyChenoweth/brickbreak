#pragma once

#include <windows.h>
#include <d3dx9core.h>
#include <boost/dynamic_bitset.hpp>

const unsigned KEY_LEFTARROW	= 0;
const unsigned KEY_RIGHTARROW	= 1;
const unsigned KEY_ESCAPE		= 2;
const unsigned MOUSE_LEFTBUTTON	= 3;

using namespace boost;

class Keyboard
{
private:
	dynamic_bitset<>	mKeyStatus;
	D3DXVECTOR3			mMousePosition;
	bool				mMouseActive;

public:
	// Constructors and Destructor
	Keyboard();
	~Keyboard();

	// Accessors and Mutators
	// mKeyStatus
	void ResetKeys();
	void PollKeys();
	void KeyDown(unsigned key);
	void KeyUp(unsigned key);
	bool IsKeyDown(unsigned key);
	// mMousePosition
	void PollMouse(HWND hwnd);
	D3DXVECTOR3 GetMousePosition();
	void SetMousePosition(D3DXVECTOR3 position);
	// mMouseActive
	bool IsMouseActive();
	void SetMouseActive();
	void SetMouseInactive();
};