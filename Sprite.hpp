#pragma once

#include <d3d9.h>
#include <D3dx9core.h>
#include <string>

using namespace std;

class Sprite
{
protected:
	ID3DXSprite*		mD3DSprite;
	IDirect3DTexture9*	mD3DTex;
	wstring				mFileName;

	D3DXVECTOR3			mPosition;

public:
	Sprite(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring fileName);
	virtual ~Sprite();

	virtual D3DXVECTOR3 GetPosition();
	virtual void SetPosition(D3DXVECTOR3 position);

	virtual void Draw();
};