#pragma once

#include "Sprite.hpp"

class Background : public Sprite
{
private:
	float		mWidth;
	float		mHeight;
	unsigned	mTilesPerRow;
	unsigned	mTilesPerColumn;

public:
	// Constructors and Destructor
	Background(IDirect3DDevice9 *d3dDevice, ID3DXSprite *d3dSprite, wstring fileName, float bgWidth, float bgHeight, float windowWidth, float windowHeight);
	~Background();

	void Draw();
};