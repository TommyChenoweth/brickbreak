#include <string>
#include "Sprite.hpp"
#include "DXUtil.hpp"

using namespace std;

Sprite::Sprite(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring fileName)
: mD3DSprite(d3dSprite),
  mFileName(fileName),
  mPosition(0, 0, 0)
{
	HRESULT createTex = D3DXCreateTextureFromFile(d3dDevice, mFileName.c_str(), &mD3DTex);
	switch(createTex)
	{
	case D3DERR_NOTAVAILABLE:
		HR(createTex);
		break;
	case D3DERR_OUTOFVIDEOMEMORY:
		ErrorBox(L"Can't create texture: Direct3D does not have enough display memory to perform the operation");
		HR(createTex);
		break;
	case D3DERR_INVALIDCALL:
		HR(createTex);
		break;
	case D3DXERR_INVALIDDATA:
		ErrorBox(L"Can't create texture: Invalid data. Check filenames.");
		HR(createTex);
		break;
	case E_OUTOFMEMORY:
		ErrorBox(L"Can't create texture: Direct3D could not allocate sufficient memory to complete the call.");
		HR(createTex);
		break;
	}
}

Sprite::~Sprite()
{
	ReleaseCOM(mD3DTex);
}

D3DXVECTOR3 Sprite::GetPosition()
{
	return mPosition;
}
void Sprite::SetPosition(D3DXVECTOR3 position)
{
	mPosition = position;
}

void Sprite::Draw()
{
	HRESULT drawSprite = mD3DSprite->Draw(
		mD3DTex,
		0,
		0,
		&mPosition,
		D3DCOLOR_XRGB(255, 255, 255));
	switch(drawSprite)
	{
	case D3DERR_INVALIDCALL:
		ErrorBox(L"Can't draw sprite: Invalid parameter(s).");
		HR(drawSprite);
		break;
	case D3DXERR_INVALIDDATA:
		ErrorBox(L"Can't draw sprite: Data corrupt/incorrect.");
		HR(drawSprite);
		break;
	}
}