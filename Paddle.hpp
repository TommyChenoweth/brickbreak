#pragma once

#include "Ball.hpp"
#include "BoundingBox.hpp"
#include "Sprite.hpp"
#include <string>
#include <vector>

using namespace std;

class Paddle : public Sprite
{
private:
	float			mWidth;
	float			mHeight;
	BoundingBox		mBounds;
	float			mSpeed;
	unsigned		mDefaultSegments;
	unsigned		mNumSegments;
	RECT			mLeftEnd;
	float			mLeftEndWidth;
	RECT			mMidSection;
	float			mMidSectionWidth;
	RECT			mRightEnd;
	float			mRightEndWidth;
	bool			mBallsLocked;
	vector<Ball*>	mLockedBalls;

public:
	// Constructors and Destructor
	Paddle(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring fileName, unsigned midSegments, float windowHeight, float windowWidth);
	~Paddle();

	// Accessors and Mutators
	// mBounds
	BoundingBox GetBounds();
	// mSpeed
	void SetSpeed(float speed);
	float GetSpeed();
	// mNumSegmets
	void SetNumSegments(unsigned numSegments);
	int GetNumSegments();
	// mLockedBalls
	void LockBallToPaddle(Ball* ball);
	void ReleaseLockedBalls();
	// mPosition
	void SetPosition(D3DXVECTOR3 position);

	void Reset();

	void Draw();
};

void PaddleMoveLeft(Paddle* paddle, float dt);
void PaddleMoveRight(Paddle* paddle, float dt);
void PaddleWallCollision(Paddle* paddle, const RECT& walls);