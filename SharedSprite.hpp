#pragma once

#include <d3d9.h>
#include <d3dx9core.h>
#include "DXUtil.hpp"
#include <string>

using namespace std;

template <typename T>
class SharedSprite
{
protected:
	static ID3DXSprite*			mD3DSprite;
	static IDirect3DTexture9*	mD3DTex;
	static wstring				mFileName;

	D3DXVECTOR3					mPosition;

public:
	// Constructors and Destructor
	SharedSprite();
	virtual ~SharedSprite();

	// Accessors and Mutators
	// mD3DTex
	static void LoadTexture(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring fileName);
	static void ReleaseTexture();
	// mPosition
	virtual D3DXVECTOR3 GetPosition();
	virtual void SetPosition(D3DXVECTOR3 position);

	virtual void Draw() = 0;
};

// Static Variables
template <typename T>
ID3DXSprite* SharedSprite<T>::mD3DSprite = 0;
template <typename T>
IDirect3DTexture9* SharedSprite<T>::mD3DTex = 0;
template <typename T>
wstring SharedSprite<T>::mFileName = L"";

// Constructors and Destructors
template <typename T>
SharedSprite<T>::SharedSprite()
: mPosition(0.0f, 0.0f, 0.0f)
{
}
template <typename T>
SharedSprite<T>::~SharedSprite()
{
}

// Accessors and Mutators
// mD3DTex
template <typename T>
void SharedSprite<T>::LoadTexture(IDirect3DDevice9* d3dDevice, ID3DXSprite* d3dSprite, wstring fileName)
{
	mFileName = fileName;
	mD3DSprite = d3dSprite;
	HRESULT createTex = D3DXCreateTextureFromFile(d3dDevice, mFileName.c_str(), &mD3DTex);
	switch(createTex)
	{
	case D3DERR_NOTAVAILABLE:
		HR(createTex);
		break;
	case D3DERR_OUTOFVIDEOMEMORY:
		ErrorBox(L"Can't create texture: Direct3D does not have enough display memory to perform the operation");
		HR(createTex);
		break;
	case D3DERR_INVALIDCALL:
		HR(createTex);
		break;
	case D3DXERR_INVALIDDATA:
		ErrorBox(L"Can't create texture: Invalid data. Check filenames.");
		HR(createTex);
		break;
	case E_OUTOFMEMORY:
		ErrorBox(L"Can't create texture: Direct3D could not allocate sufficient memory to complete the call.");
		HR(createTex);
		break;
	}
}
template <typename T>
void SharedSprite<T>::ReleaseTexture()
{
	ReleaseCOM(mD3DTex);
}
// mPosition
template <typename T>
D3DXVECTOR3 SharedSprite<T>::GetPosition()
{
	return mPosition;
}
template <typename T>
void SharedSprite<T>::SetPosition(D3DXVECTOR3 position)
{
	mPosition = position;
}