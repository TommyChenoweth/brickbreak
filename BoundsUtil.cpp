#include <math.h>
#include <d3dx9core.h>
#include <utility>
#include <cstdio>
#include <windows.h>
#include <vector>
#include "LineEquation.hpp"
#include "CircleEquation.hpp"
#include "Root.hpp"
#include "BoundsUtil.hpp"
#include "BoundingSphere.hpp"
#include "BoundingBox.hpp"
#include "Solution.hpp"
#include <boost/math/special_functions/fpclassify.hpp>
#include <boost/math/special_functions/pow.hpp>
#include <boost/algorithm/minmax.hpp>
#include <boost/tuple/tuple.hpp>

using namespace std;
using namespace boost::tuples;
using namespace boost::math;

template <typename T>
bool ValueInRange(T value, T rangeMin, T rangeMax);
D3DXVECTOR3 FindClosestCorner(const D3DXVECTOR3& pt, const BoundingBox& box);
bool Collision_Static_CircleLineSegment(const D3DXVECTOR3& lineSegBegin, const D3DXVECTOR3& lineSegEnd, const BoundingSphere& sphere, vector<D3DXVECTOR3>& outIntersectionPts);
pair<D3DXVECTOR3, D3DXVECTOR3> FindLineCircleIntersection(const LineEquation& line, const CircleEquation& circle);
pair<Root, Root> EvaluateQuadratic(double a, double b, double c);
float DetermineDistance(const D3DXVECTOR3& pointA, const D3DXVECTOR3& pointB);

template <typename T>
bool ValueInRange(T value, T rangeMin, T rangeMax)
{
	if(rangeMin <= value && value <= rangeMax)
		return true;
	return false;
}

D3DXVECTOR3 FindClosestCorner(const D3DXVECTOR3& pt, const BoundingBox& box)
{
	float yDistFromCenter(box.GetCenter().y - pt.y);
	float xDistFromCenter(box.GetCenter().x - pt.x);
	D3DXVECTOR3 corner(0.0f, 0.0f, 0.0f);
	0 < yDistFromCenter ? corner.y = box.GetTopSide() : corner.y = box.GetBottomSide();
	0 < xDistFromCenter ? corner.x = box.GetLeftSide() : corner.x = box.GetRightSide();
	return corner;
}

bool Collision_Static_CircleLineSegment(const D3DXVECTOR3& lineSegBegin, const D3DXVECTOR3& lineSegEnd, const BoundingSphere& sphere, vector<D3DXVECTOR3>& outIntersectionPts)
{
	// Construct a line to represt the line segment.
	LineEquation line(1.0f, 1.0f);
	line.SetM(DetermineSlope(lineSegBegin, lineSegEnd));
	line.SetY(lineSegBegin.y);
	line.SetX(lineSegBegin.x);
	line.SetB(SolveForB(line));

	wprintf(L"Line: Y = (%f)X + %f\n", line.GetM(), line.GetB());

	// Construct a circle to represent the bounding sphere.
	CircleEquation circle(1.0f, 0.0f, 0.0f);
	circle.SetR(sphere.GetRadius());
	circle.SetH(sphere.GetCenter().x);
	circle.SetK(sphere.GetCenter().y);

	wprintf(L"Circle: (%f)^2 = (X-%f)^2 + (Y-%f)^2\n", circle.GetR(), circle.GetH(), circle.GetK());

	// Is the slope of the line segment infinite?
	if(boost::math::isinf<float>(line.GetM()))
	{
		// The slope of the line segment is infinite, so intersection
		// can't occur unless the line segment is vertically alligned with the
		// circle.
		float circleLeft(sphere.GetCenter().x - sphere.GetRadius());
		float circleRight(sphere.GetCenter().x + sphere.GetRadius());
		bool verticallyAlligned(ValueInRange(lineSegBegin.x, circleLeft, circleRight));
		if(!verticallyAlligned)
			return 0;

		// Determine the range of the line segment.
		tuple<float, float> lineRange = boost::minmax(lineSegBegin.y, lineSegEnd.y);

		// Find the collision points of the line segment.
		circle.SetX(lineSegBegin.x);
		pair<Solution, Solution> yIntercepts = SolveForY(circle);

		// Check both collision points to ensure that they exist and are in range.
		if(yIntercepts.first.IsReal() && ValueInRange(yIntercepts.first.GetSolution(), lineRange.get<0>(), lineRange.get<1>()))
			outIntersectionPts.push_back(D3DXVECTOR3(lineSegBegin.x, yIntercepts.first.GetSolution(), 0.0f));
		if(yIntercepts.second.IsReal() && ValueInRange(yIntercepts.second.GetSolution(), lineRange.get<0>(), lineRange.get<1>()))
			outIntersectionPts.push_back(D3DXVECTOR3(lineSegBegin.x, yIntercepts.second.GetSolution(), 0.0f));

		if(0 < outIntersectionPts.size())
			return true;
		return false;
	}

	double a(1 + pow<2>(static_cast<double>(line.GetM())));
	double b(-2.0*circle.GetH() + 2.0*line.GetM()*line.GetB() - 2.0*line.GetM()*circle.GetK());
	double c(-2.0*static_cast<double>(line.GetB())*static_cast<double>(circle.GetK()) + pow<2>(static_cast<double>(line.GetB())) + pow<2>(static_cast<double>(circle.GetK())) + pow<2>(static_cast<double>(circle.GetH())) - pow<2>(static_cast<double>(circle.GetR())));

	wprintf(L"Quadratic:\n\ta - %f\n\tb - %f\n\tc - %f\n", a, b, c);

	pair<Root, Root> roots = EvaluateQuadratic(a, b, c);

	wprintf(L"Roots:\n\t%f\n\t%f\n", roots.first.GetValue(), roots.second.GetValue());

	LineEquation tmpLine(line);

	// Determine the domain of the line segment.
	tuple<float, float> lineDomain = boost::minmax(lineSegBegin.x, lineSegEnd.x);

	if(roots.first.IsReal() && ValueInRange(roots.first.GetValue(), lineDomain.get<0>(), lineDomain.get<1>()))
	{
		tmpLine.SetX(roots.first.GetValue());
		tmpLine.SetY(SolveForY(tmpLine));
		outIntersectionPts.push_back(D3DXVECTOR3(tmpLine.GetX(), tmpLine.GetY(), 0.0f));
	}
	if(roots.second.IsReal() && ValueInRange(roots.second.GetValue(), lineDomain.get<0>(), lineDomain.get<1>()))
	{
		tmpLine.SetX(roots.second.GetValue());
		tmpLine.SetY(SolveForY(tmpLine));
		outIntersectionPts.push_back(D3DXVECTOR3(tmpLine.GetX(), tmpLine.GetY(), 0.0f));
	}

	if(0 < outIntersectionPts.size())
		return true;
	return false;
}

bool Collision_Sweep_CircleSquare(const BoundingSphere& bCircleBegin, const BoundingSphere& bCircleEnd, const BoundingBox& bBox, D3DXVECTOR3& outIntersectPt, D3DXVECTOR3& outRestingPt)
{
	D3DXVECTOR3 lineSegBegin(bCircleBegin.GetCenter());
	D3DXVECTOR3 lineSegEnd(bCircleEnd.GetCenter());

	float radius(bCircleBegin.GetRadius());

	// Create an outer box that blah blah blah
	BoundingBox oBox(bBox.GetLeftSide() - radius, bBox.GetTopSide() - radius, bBox.GetWidth() + 2 * radius, bBox.GetHeight() + 2 * radius);

	// Find all points at which the constructed line segment and box intersect.
	vector<D3DXVECTOR3> oBoxIPts;
	if(!Collision_Static_LineSegmentSquare(lineSegBegin, lineSegEnd, oBox, oBoxIPts))
		return false;

	wprintf(L"bCircleBegin:\n\t(%.15f, %.15f)\n", bCircleBegin.GetCenter().x, bCircleBegin.GetCenter().y);
	wprintf(L"bCircleEnd:\n\t(%.15f, %.15f)\n", bCircleEnd.GetCenter().x, bCircleEnd.GetCenter().y);
	wprintf(L"bBox:\n\tLeft: %f\n\tTop: %f\n\tRight: %f\n\tBottom: %f\n", bBox.GetLeftSide(), bBox.GetTopSide(), bBox.GetRightSide(), bBox.GetBottomSide());
		
	// Which box intersection is closest to the beginning sphere?
	D3DXVECTOR3* oBoxIPt(0);
	D3DXVECTOR3 corner(0.0f, 0.0f, 0.0f);
	if(0 < oBoxIPts.size())
	{
		float shortestDistToBegSphere(FLT_MAX);
		for(unsigned i = 0; i < oBoxIPts.size(); i++)
		{
			float currDistToBegSphere(D3DXVec3Length(&(bCircleBegin.GetCenter() - oBoxIPts[i])));
			if(currDistToBegSphere < shortestDistToBegSphere)
			{
				shortestDistToBegSphere = currDistToBegSphere;
				oBoxIPt = &oBoxIPts[i];
			}
		}
		
		// Which corner is that intersection point closest to?
		float yDistFromCenter(bBox.GetCenter().y - oBoxIPt->y);
		float xDistFromCenter(bBox.GetCenter().x - oBoxIPt->x);
		0 < yDistFromCenter ? corner.y = bBox.GetTopSide() : corner.y = bBox.GetBottomSide();
		0 < xDistFromCenter ? corner.x = bBox.GetLeftSide() : corner.x = bBox.GetRightSide();
	}
	else
	{
		// Which corner is the beginning sphere closest to?
		float yDistFromCenter(bBox.GetCenter().y - bCircleBegin.GetCenter().y);
		float xDistFromCenter(bBox.GetCenter().x - bCircleBegin.GetCenter().x);
		0 < yDistFromCenter ? corner.y = bBox.GetTopSide() : corner.y = bBox.GetBottomSide();
		0 < xDistFromCenter ? corner.x = bBox.GetLeftSide() : corner.x = bBox.GetRightSide();
	}
	
	// Build a bounding sphere for that corner and test it for collision against the line segment.
	BoundingSphere cCircle(corner, radius);
	vector<D3DXVECTOR3> cCircleIPts;
	D3DXVECTOR3* cCircleIPt(0);
	if(!Collision_Static_CircleLineSegment(lineSegBegin, lineSegEnd, cCircle, cCircleIPts))
		cCircleIPt = 0;
	else
	{
		// Find the closest sphere intersection point.
		float cCircleIPtShortestDist(FLT_MAX);
		for(unsigned i = 0; i < cCircleIPts.size(); i++)
		{
			float dist(D3DXVec3Length(&(cCircleIPts[i] - bCircleBegin.GetCenter())));
			if(dist < cCircleIPtShortestDist)
			{
				cCircleIPtShortestDist = dist;
				cCircleIPt = &cCircleIPts[i];
			}
		}
	}
		
	// Remove intersection points that are on the corners of the box.
	vector<D3DXVECTOR3>::iterator oBoxIt = oBoxIPts.begin();
	while(oBoxIt != oBoxIPts.end())
	{
		bool xAtCorner(ValueInRange(oBoxIt->x, oBox.GetLeftSide(), oBox.GetLeftSide() + radius) || ValueInRange(oBoxIt->x, oBox.GetRightSide() - radius, oBox.GetRightSide()));
		bool yAtCorner(ValueInRange(oBoxIt->y, oBox.GetTopSide(), oBox.GetTopSide() + radius) || ValueInRange(oBoxIt->y, oBox.GetBottomSide() - radius, oBox.GetBottomSide()));
		if(xAtCorner && yAtCorner)
		{
			// The following comparison is bad because the memory that oBoxIPt refers to
			// may have been deallocated if the vector has been resized. Think of something better.
			if(oBoxIPt && (*oBoxIt) == (*oBoxIPt))
				oBoxIPt = 0;
			oBoxIPts.erase(oBoxIt);
			oBoxIt = oBoxIPts.begin();
			continue;
		}
		else if(!oBoxIPt)
			oBoxIPt = &(*oBoxIt);
		oBoxIt++;
	}

	bool useCCircleIPt(false);
	bool useOBoxIPt(false);
	if(cCircleIPt && oBoxIPt)
	{
		// Is the sphere or box intersection closer to the beginning sphere?
		float sphereDist(D3DXVec3Length(&((*cCircleIPt) - bCircleBegin.GetCenter())));
		float boxDist(D3DXVec3Length(&((*oBoxIPt) - bCircleBegin.GetCenter())));
		wprintf(L"SP: (%f, %f)\n", (*cCircleIPt).x, (*cCircleIPt).y);
		wprintf(L"CP: (%f, %f)\n", corner.x, corner.y);
		wprintf(L"BP: (%f, %f)\n", (*oBoxIPt).x, (*oBoxIPt).y);
		wprintf(L"SD: %f vs BD: %f\n", sphereDist, boxDist);
		if(sphereDist <= boxDist)
			useCCircleIPt = true;
		else
			useOBoxIPt = true;
	}
	else if(cCircleIPt)
		useCCircleIPt = true;
	else if(oBoxIPt)
		useOBoxIPt = true;
		
	if(useCCircleIPt)
	{
		outIntersectPt = corner;
		outRestingPt = (*cCircleIPt);
		wprintf(L"Corner collision: (%f, %f)\n", corner.x, corner.y);
		wprintf(L"RestingPt: (%f, %f)\n", outRestingPt.x, outRestingPt.y);
		return true;
	}
	else if(useOBoxIPt)
	{
		// Either the box intersection is closer to the beginning sphere, or there are no sphere
		// intersection points. Either way, the beginning sphere collided with the outer box.
		// If it collided into the top side...
		if(oBoxIPt->y <= bBox.GetTopSide())
		{
			// ...then the point of collision is radius units under the ball's center point.
			wprintf(L"Top side collision.\n");
			outIntersectPt.x = oBoxIPt->x;
			outIntersectPt.y = oBoxIPt->y + radius;
		}
		// If it collided into the bottom side...
		else if(bBox.GetBottomSide() <= oBoxIPt->y)
		{
			// ...then the point of collision is radius units above the ball's center point.
			wprintf(L"Bottom side collision.\n");
			outIntersectPt.x = oBoxIPt->x;
			outIntersectPt.y = oBoxIPt->y - radius;
		}
		// If it collided into the left side...
		else if(oBoxIPt->x <= bBox.GetLeftSide())
		{
			// ...then the point of collision is radius units to the right of the ball's center point.
			wprintf(L"Left side collision.\n");
			outIntersectPt.x = oBoxIPt->x + radius;
			outIntersectPt.y = oBoxIPt->y;
		}
		// If it collided into the rigth side...
		else if(bBox.GetRightSide() <= oBoxIPt->x)
		{
			// ...then the point of collision is radius units to the left of the ball's center point.
			wprintf(L"Right side collision.\n");
			outIntersectPt.x = oBoxIPt->x - radius;
			outIntersectPt.y = oBoxIPt->y;
		}
		outRestingPt = (*oBoxIPt);
		wprintf(L"RestingPt: (%f, %f)\n", outRestingPt.x, outRestingPt.y);
		return true;
	}

	wprintf(L"FALSE ALARM!\n");
	return false;
}
bool SphereIntersectsSphere(const BoundingSphere& bSphereA, const BoundingSphere& bSphereB)
{
	static D3DXVECTOR3 vecAToB(0.0f, 0.0f, 0.0f);
	vecAToB = bSphereA.GetCenter() - bSphereB.GetCenter();

	static float distAToB(0.0f);
	distAToB = D3DXVec3Length(&vecAToB);

	if(distAToB <= bSphereA.GetRadius() + bSphereB.GetRadius()) return true;
	return false;
}
int SphereIntersectsBox(const BoundingSphere& bSphere, const BoundingBox& bBox, const D3DXVECTOR3& direction)
{
	float distX(0.0f);
	distX = fabs(bSphere.GetCenter().x - bBox.GetCenter().x);

	float distY(0.0f);
	distY = fabs(bSphere.GetCenter().y - bBox.GetCenter().y);

	float halfBoxWidth(0.0f);
	halfBoxWidth = bBox.GetWidth() * 0.5f;

	float halfBoxHeight(0.0f);
	halfBoxHeight = bBox.GetHeight() * 0.5f;

	float sphereRadius(0.0f);
	sphereRadius = bSphere.GetRadius();

	bool possibleVerticalCollision(false);
	possibleVerticalCollision = (distY <= halfBoxHeight + sphereRadius);
	bool verticallyAligned(false);
	verticallyAligned = (distX <= halfBoxWidth);

	bool possibleHorizontalCollision(false);
	possibleHorizontalCollision = (distX <= halfBoxWidth + sphereRadius);
	bool horizontallyAligned(false);
	horizontallyAligned = (distY <= halfBoxHeight);

	// Check for vertical collision.
	if(possibleVerticalCollision && verticallyAligned)
	{
		return bSphere.GetCenter().y < bBox.GetCenter().y ? 1 : 2;
	}

	// Check for horizontal collision.
	if(possibleHorizontalCollision && horizontallyAligned)
	{
		return bSphere.GetCenter().x < bBox.GetCenter().x ? 3 : 4;
	}

	// Check for corner collision.
	if(possibleHorizontalCollision && possibleVerticalCollision)
	{
		return 5;
	}

	// No collision was found.
	return 0;
}

pair<D3DXVECTOR3, D3DXVECTOR3> FindLineCircleIntersection(const LineEquation& line, const CircleEquation& circle)
{
	float a(0.0f);
	float b(0.0f);
	float c(0.0f);

	a = 1 + pow(line.GetM(), 2);
	b = -2*circle.GetH() + 2*line.GetM()*line.GetB() - 2*line.GetM()*circle.GetK();
	c = -2*line.GetB()*circle.GetK() + pow(line.GetB(), 2) + pow(circle.GetK(), 2) + pow(circle.GetH(), 2) - pow(circle.GetR(), 2);

	pair<Root, Root> roots = EvaluateQuadratic(a, b, c);

	LineEquation tmpLine(line);

	D3DXVECTOR3 ptOfCollisionA(0.0f, 0.0f, 0.0f);
	if(roots.first.IsReal())
	{
		tmpLine.SetX(roots.first.GetValue());
		tmpLine.SetY(SolveForY(tmpLine));
		ptOfCollisionA.x = tmpLine.GetX();
		ptOfCollisionA.y = tmpLine.GetY();
	}

	D3DXVECTOR3 ptOfCollisionB(0.0f, 0.0f, 0.0f);
	if(roots.second.IsReal())
	{
		tmpLine.SetX(roots.second.GetValue());
		tmpLine.SetY(SolveForY(tmpLine));
		ptOfCollisionB.x = tmpLine.GetX();
		ptOfCollisionB.y = tmpLine.GetY();
	}

	pair<D3DXVECTOR3, D3DXVECTOR3> ptsOfCollision(ptOfCollisionA, ptOfCollisionB);

	return ptsOfCollision;
}

pair<Root, Root> EvaluateQuadratic(double a, double b, double c)
{
	pair<Root, Root> roots(Root(0.0f, true), Root(0.0f, true));
	double tmp = pow(static_cast<double>(b), 2.0) - (4.0*static_cast<double>(a)*static_cast<double>(c));
	wprintf(L"tmp - %f\n", tmp);
	if(tmp < 0.0)
	{
		roots.first.SetImaginary();
		roots.second.SetImaginary();
	}
	else
	{
		roots.first.SetValue((-b + sqrt(static_cast<double>(tmp))) / (2.0*a));
		roots.second.SetValue((-b - sqrt(static_cast<double>(tmp))) / (2.0*a));
	}
	return roots;
}

float DetermineDistance(const D3DXVECTOR3& pointA, const D3DXVECTOR3& pointB)
{
	return sqrt(pow(pointB.x - pointA.x, 2) + pow(pointB.y - pointA.y, 2));
}

bool BoxIntersectsBox(const BoundingBox& bBoxA, const BoundingBox& bBoxB)
{
	static float distX(0.0f);
	distX = fabs(bBoxA.GetCenter().x - bBoxB.GetCenter().x);
	static float distY(0.0f);
	distY = fabs(bBoxA.GetCenter().y - bBoxB.GetCenter().y);

	static float halfBoxAWidth(0.0f);
	halfBoxAWidth = bBoxA.GetWidth() * 0.5f;
	static float halfBoxAHeight(0.0f);
	halfBoxAHeight = bBoxA.GetHeight() * 0.5f;
	
	static float halfBoxBWidth(0.0f);
	halfBoxBWidth = bBoxB.GetWidth() * 0.5f;
	static float halfBoxBHeight(0.0f);
	halfBoxBWidth = bBoxB.GetWidth() * 0.5f;

	static bool possibleVerticalCollision(false);
	possibleVerticalCollision = (distY <= halfBoxAHeight + halfBoxBHeight);
	static bool verticallyAlligned(false);
	verticallyAlligned = (distX <= halfBoxAWidth);

	if(possibleVerticalCollision && verticallyAlligned)
		return true;

	static bool possibleHorizontalCollision(false);
	possibleHorizontalCollision = (distX <= halfBoxAWidth + halfBoxBWidth);
	static bool horizontallyAlligned(false);
	horizontallyAlligned = (distY <= halfBoxAHeight);

	if(possibleHorizontalCollision && horizontallyAlligned)
		return true;

	return false;
}

bool Collision_Static_LineSegmentSquare(const D3DXVECTOR3& lineSegBegin, const D3DXVECTOR3& lineSegEnd, const BoundingBox& box, vector<D3DXVECTOR3>& outIntersectionPts)
{
	// Determine the domain of the line segment.
	float minX(0.0f);
	float maxX(0.0f);
	if(lineSegBegin.x <= lineSegEnd.x)
	{
		minX = lineSegBegin.x;
		maxX = lineSegEnd.x;
	}
	else
	{
		minX = lineSegEnd.x;
		maxX = lineSegBegin.x;
	}

	// Determine the range of the line segment.
	float minY(0);
	float maxY(0);
	if(lineSegBegin.y <= lineSegEnd.y)
	{
		minY = lineSegBegin.y;
		maxY = lineSegEnd.y;
	}
	else
	{
		minY = lineSegEnd.y;
		maxY = lineSegBegin.y;
	}

	bool verticallyAlligned(false);
	float boxRight(box.GetRightSide());
	float boxLeft(box.GetLeftSide());
	if(minX <= boxLeft && boxLeft <= maxX)
		verticallyAlligned = true;
	else if(boxLeft <= minX && minX <= boxRight)
		verticallyAlligned = true;

	if(!verticallyAlligned)
		return false;

	bool horizontallyAlligned(false);
	float boxTop(box.GetTopSide());
	float boxBottom(box.GetBottomSide());
	if(minY <= boxTop && boxTop <= maxY)
		horizontallyAlligned = true;
	else if(boxTop <= minY && minY <= boxBottom)
		horizontallyAlligned = true;
	
	if(!horizontallyAlligned)
		return false;

	LineEquation line(1.0f, 1.0f);
	line.SetM(DetermineSlope(lineSegBegin, lineSegEnd));
	line.SetX(lineSegBegin.x);
	line.SetY(lineSegBegin.y);
	line.SetB(SolveForB(line));

	// Is the slope of the line segment infinite?
	if(boost::math::isinf<float>(line.GetM()))
	{
		// The slope is infinite. This means that the line segment is completely
		// vertical, so there cannot be any collision if the line segment isn't vertically
		// alligned.
		if(!verticallyAlligned)
			return false;

		// Because the line segment is vertical, we only need to check the top and
		// bottom of the box for collision.
		if(ValueInRange(box.GetTopSide(), minY, maxY))
			outIntersectionPts.push_back(D3DXVECTOR3(lineSegBegin.x, box.GetTopSide(), 0.0f));
		if(ValueInRange(box.GetBottomSide(), minY, maxY))
			outIntersectionPts.push_back(D3DXVECTOR3(lineSegBegin.x, box.GetBottomSide(), 0.0f));
			
		if(0 < outIntersectionPts.size())
			return true;
		if(horizontallyAlligned && verticallyAlligned)
			return true;
		return false;
	}

	if(verticallyAlligned)
	{
		if(minX < box.GetLeftSide())
			minX = box.GetLeftSide();
		if(box.GetRightSide() < maxX)
			maxX = box.GetRightSide();

		line.SetY(box.GetTopSide());
		float xIntersect(0);
		xIntersect = SolveForX(line);
		//wprintf(L"Top: %f\n", xIntersect);
		if(ValueInRange(xIntersect, minX, maxX) && ValueInRange(line.GetY(), minY, maxY))
			outIntersectionPts.push_back(D3DXVECTOR3(xIntersect, line.GetY(), 0.0f));

		line.SetY(box.GetBottomSide());
		xIntersect = 0;
		xIntersect = SolveForX(line);
		//wprintf(L"Bottom: %f\n", xIntersect);
		if(ValueInRange(xIntersect, minX, maxX) && ValueInRange(line.GetY(), minY, maxY))
			outIntersectionPts.push_back(D3DXVECTOR3(xIntersect, line.GetY(), 0.0f));
	}

	if(horizontallyAlligned)
	{
		if(minY < box.GetTopSide())
			minY = box.GetTopSide();
		if(box.GetBottomSide() < maxY)
			maxY = box.GetBottomSide();

		line.SetX(box.GetLeftSide());
		float yIntersect(0);
		yIntersect = SolveForY(line);
		//wprintf(L"Left: %f\n", yIntersect);
		if(ValueInRange(yIntersect, minY, maxY) && ValueInRange(line.GetX(), minX, maxX))
			outIntersectionPts.push_back(D3DXVECTOR3(line.GetX(), yIntersect, 0.0f));

		line.SetX(box.GetRightSide());
		yIntersect = 0;
		yIntersect = SolveForY(line);
		//wprintf(L"Right: %f\n", yIntersect);
		if(ValueInRange(yIntersect, minY, maxY) && ValueInRange(line.GetX(), minX, maxX))
			outIntersectionPts.push_back(D3DXVECTOR3(line.GetX(), yIntersect, 0.0f));
	}
	
	if(0 < outIntersectionPts.size())
		return true;
	else if(outIntersectionPts.size() == 0 && horizontallyAlligned && verticallyAlligned)
		return true;

	return false;
}